﻿

Notes
=====

To get started, you could visit localhost:portnumber to get a preview of the visitor section, and localhost:portnumber/admin to get a preview of the admin section.

Create a page in the admin section : 
-------

Add a new view and in the top section, insert this code : 

    @ {
		Layout = "~/Views/Shared/_AdminLayout.cshtml";
	}
	// Your html code goes here

Create a page in the visitor section : 
-------

Add a new view and in the top section, insert this code : 

    @ {
		Layout = "~/Views/Shared/_ClientLayout.cshtml";
	}
	// Your html code goes here

**PS : Don't forget to update your database credentials (username/password) in the Web.config file**

Happy Hunting !!
----------------