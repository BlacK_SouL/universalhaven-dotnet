﻿using Domain.Models;
using ServicePattern;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Universalhaven.Data.Infrastructure;

namespace Service
{
    public class FundraisingEventService:Service<fundraisingevent>,IFundraisingEventService
    {
        private static DatabaseFactory Dbf = new DatabaseFactory();
        private static UnitOfWork utw = new UnitOfWork(Dbf);
        public FundraisingEventService() : base(utw)
        {

        }

        public IEnumerable<fundraisingevent> GetEventsByUser(long id)
        {
            return utw.GetRepository<fundraisingevent>().GetMany(f => f.person.id.Equals(id));
        }


    }
}
