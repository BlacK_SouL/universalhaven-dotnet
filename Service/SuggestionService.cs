﻿using Domain.Models;
using ServicePattern;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Universalhaven.Data.Infrastructure;

namespace Service
{
    public class SuggestionService : Service<suggestion>, ISuggestionService
    {
        private static DatabaseFactory Dbf = new DatabaseFactory();
        private static UnitOfWork utw = new UnitOfWork(Dbf);
        public SuggestionService() : base(utw)
        {

        }

        public IEnumerable<suggestion> GetSuggestionsByUser(long id)
        {
            return utw.GetRepository<suggestion>().GetMany(s => s.person.id.Equals(id));
            
        }
        public IEnumerable<suggestion> GetSuggestionsByCamp(long id)
        {
            return utw.GetRepository<suggestion>().GetMany(s => s.camp.id.Equals(id));

        }
    }
}
