﻿using Data;
using Domain.Models;
using ServicePattern;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Universalhaven.Data.Infrastructure;

namespace Service
{
    public class RefugeeService : Service<person> ,IRefugeeService
    {
        private static DatabaseFactory Dbf = new DatabaseFactory();
        private static UnitOfWork utw = new UnitOfWork(Dbf);

        public RefugeeService() : base(utw)
        {
           
        }
        public int GetRefugeePer(string refugeegender)
        {
            refugeegender = refugeegender.ToLower();
            return Dbf.DataContext.getDatabase().ExecuteSqlCommand("update person p set p.type='refugee' where p.gender='"+refugeegender+"' and " +
                " p.type='refugee'");

           
        }

        

    }
}
