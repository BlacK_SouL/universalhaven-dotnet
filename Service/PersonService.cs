﻿using Domain.Models;
using ServicePattern;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Universalhaven.Data.Infrastructure;

namespace Service
{
    public class PersonService : Service<person>, IPersonService
    {
        private static DatabaseFactory Dbf = new DatabaseFactory();
        private static UnitOfWork utw = new UnitOfWork(Dbf);
        public PersonService() : base(utw)
        {

        }
    }
}
