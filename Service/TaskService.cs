﻿using Domain.Models;
using ServicePattern;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Universalhaven.Data.Infrastructure;
using System.Linq.Expressions;

namespace Service
{
    public class StarredUser
    {
        public long Id { get; set; }
        public int completedtasks { get; set; }
        public int onTime { get; set; }
        public int afterDeadline { get; set; }
        public string picture;
        public string login;
    }
    public class TaskStat
    {   
        public int pending;
        public int doing;
        public int done;
        public int declined;
        public int ontime;
        public int afterdeadline;
        public int total;
        public List<StarredUser> topthree = new List<StarredUser>();
        
    }
    public class TaskLists
    {
        public IEnumerable<Domain.Models.Task> pending { get; set; }
        public IEnumerable<Domain.Models.Task> doing { get; set; }
        public IEnumerable<Domain.Models.Task> done { get; set; }
        public IEnumerable<Domain.Models.Task> declined { get; set; }
        
    }
    public class TaskService : Service<Domain.Models.Task>, ITaskService
    {
        private static DatabaseFactory Dbf = new DatabaseFactory();
        private static UnitOfWork utw = new UnitOfWork(Dbf);
        
        public TaskService() : base(utw)
        {
             
        }
       
        public override void Add(Domain.Models.Task entity)
        {
            
            
            base.Add(entity);
        }

        public void GetAssignedTasks(long id, long campid, string role, TaskLists tasks)
        {
            if (role.Contains("CAMP_MANAGER"))
            {
                tasks.pending = GetMany(task => task.TaskAssignerId == id && task.Status == Domain.Models.TaskStatus.PENDING && task.campId == campid);
                tasks.doing = GetMany(task => task.TaskAssignerId == id && task.Status == Domain.Models.TaskStatus.DOING && task.campId == campid);
                tasks.done = GetMany(task => task.TaskAssignerId == id && task.Status == Domain.Models.TaskStatus.DONE && task.campId == campid);
                tasks.declined = GetMany(task => task.TaskAssignerId == id && task.Status == Domain.Models.TaskStatus.DECLINED && task.campId == campid);
            }
            else if (role.Contains("STAFF"))
            {
               
                
                tasks.pending = GetMany(task => task.TaskExecutorId == id && task.Status == Domain.Models.TaskStatus.PENDING && task.campId == campid);
                tasks.doing = GetMany(task => task.TaskExecutorId == id && task.Status == Domain.Models.TaskStatus.DOING && task.campId == campid);
                tasks.done = GetMany(task => task.TaskExecutorId == id && task.Status == Domain.Models.TaskStatus.DONE && task.campId == campid);
                tasks.declined = GetMany(task => task.TaskExecutorId == id && task.Status == Domain.Models.TaskStatus.DECLINED && task.campId == campid);
            }
            
        }

        public void changeStatus(long taskid, string status)
        {
            Domain.Models.Task t = GetById(taskid);
            t.Status = (Domain.Models.TaskStatus)Enum.Parse(typeof(Domain.Models.TaskStatus), status, true);
            if (t.Status.Equals(Domain.Models.TaskStatus.DONE))
            {
                t.EndingDate = DateTime.Now;
            }
            else
            {
                t.EndingDate = null;
            }
            Update(t);
            Commit();
        }

        public TaskStat GetTaskStats(long campid)
        {
            IEnumerable<Domain.Models.Task> total = GetAll();
            IEnumerable<Domain.Models.Task> pendingTasks = total.Where(t => t.campId == campid && t.Status== Domain.Models.TaskStatus.PENDING);
            IEnumerable<Domain.Models.Task> doingTasks = GetMany(t => t.campId == campid && t.Status==Domain.Models.TaskStatus.DOING);
            IEnumerable<Domain.Models.Task> doneTasks = GetMany(t => t.campId == campid && t.Status==(Domain.Models.TaskStatus.DONE));
            IEnumerable<Domain.Models.Task> declined = GetMany(t => t.campId == campid && t.Status==(Domain.Models.TaskStatus.DECLINED));
            IEnumerable<Domain.Models.Task> tasksOnTime = GetMany(t => t.campId == campid && t.EndingDate <= t.DeadLine && t.Status == Domain.Models.TaskStatus.DONE);
            IEnumerable<Domain.Models.Task> tasksAfterDeadline = GetMany(t => t.campId == campid && t.EndingDate > t.DeadLine && t.Status == Domain.Models.TaskStatus.DONE);

            Dictionary<long, int> userScores = new Dictionary<long, int>();

            foreach (var task in doneTasks)
            {
                var userid= (long)task.TaskExecutorId;
                if (userScores.ContainsKey(userid) == false)
                {
                    userScores.Add(userid, 0);
                }
                if (task.EndingDate <= task.DeadLine)
                    userScores[userid] += 2;
                else
                    userScores[userid] += 1;
            }
            userScores.OrderBy(e => e.Value);

            List<long> ids = userScores.Keys.ToList() ;
            //ids.Reverse();
            int count= ids.Count() >= 3 ? 3 : ids.Count();
            List<StarredUser> starredUsers = new List<StarredUser>();
            for (int i = 0; i < count; i++)
            {
                var starred = new StarredUser()
                {
                    Id = ids[i],
                    completedtasks = doneTasks.Where(t => t.TaskExecutorId == ids[i]).Count(),
                    onTime = doneTasks.Where(t => t.EndingDate <= t.DeadLine && t.TaskExecutorId == ids[i]).Count(),
                    afterDeadline = doneTasks.Where(t => t.EndingDate > t.DeadLine && t.TaskExecutorId == ids[i]).Count()
                };
                starredUsers.Add(starred);
            }
            return new TaskStat
            {
                pending = pendingTasks.Count(),
                doing = doingTasks.Count(),
                done = doneTasks.Count(),
                declined = declined.Count(),
                ontime= tasksOnTime.Count(),
                afterdeadline= tasksAfterDeadline.Count(),
                total= total.Count(),
                topthree= starredUsers
            };

        }
    }
}
