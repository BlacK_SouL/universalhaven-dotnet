﻿using ServicePattern;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Universalhaven.Data.Infrastructure;
using Domain.Models;

namespace Service
{
    public class TaskCommentService : Service<Domain.Models.TaskComment>, ITaskCommentService
    {
        private static DatabaseFactory Dbf = new DatabaseFactory();
        private static UnitOfWork utw = new UnitOfWork(Dbf);

        public TaskCommentService() : base(utw)
        {
             
        }

        public override void Delete(TaskComment entity)
        {
           
            base.Delete(entity);
            base.Commit();
            

        }
    }
}
