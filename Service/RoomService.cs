﻿using Domain.Models;
using ServicePattern;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Universalhaven.Data.Infrastructure;

namespace Service
{
    public class RoomService : Service<room> , IRoomService
    {
        private static DatabaseFactory Dbf = new DatabaseFactory();
        private static UnitOfWork utw = new UnitOfWork(Dbf);

        public RoomService() : base (utw)
        {

        }

        public IEnumerable<room> GetAvailableRooms(int familynbr, string bedtype)
        {
            typeroom type = (typeroom)Enum.Parse(typeof(typeroom), bedtype, true);
            return GetMany(r => r.remainingbeds>=familynbr && r.Type==type);
        }
        public IEnumerable<room> GetRoomperType(string roomtype)
        {
            typeroom type = (typeroom)Enum.Parse(typeof(typeroom), roomtype, true);
            return GetMany(r => r.Type == type);
        }
        public void updateBeds(int id, int familynbr)
        {

           var room = GetById(id);
            room.remainingbeds -= familynbr;
            Update(room);
            Commit();
        }
    }
}
