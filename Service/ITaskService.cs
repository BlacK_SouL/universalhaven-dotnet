﻿using Domain.Models;
using ServicePattern;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Service
{
    public interface ITaskService : IService <Domain.Models.Task>
    {
        void GetAssignedTasks (long id, long campid, string role, TaskLists tasks);
        TaskStat GetTaskStats(long campid);
        void changeStatus(long taskid, string status);
    }
}
