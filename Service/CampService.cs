﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ServicePattern;
using Domain.Models;
using Universalhaven.Data.Infrastructure;

namespace Service
{
    public class CampService : Service<camp>, ICampService
    {
        private static DatabaseFactory Dbf = new DatabaseFactory();
        private static UnitOfWork utw = new UnitOfWork(Dbf);
        public CampService() : base(utw)
        {

        }
        public void updateCampscapacity(long id, int familynbr)
        {

            var camp = GetById(id);
            camp.capacity -= familynbr;
            Update(camp);
            Commit();
        }
    }
}
