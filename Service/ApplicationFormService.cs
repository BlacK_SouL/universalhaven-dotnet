﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ServicePattern;
using Domain.Models;
using Universalhaven.Data.Infrastructure;

namespace Service
{
    public class ApplicationFormService : Service<applicationform>, IApplicationFrom
    {
        private static DatabaseFactory Dbf = new DatabaseFactory();
        private static UnitOfWork utw = new UnitOfWork(Dbf);
        public ApplicationFormService() : base(utw)
        {

        }
    }
}
