﻿using Domain.Models;
using ServicePattern;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Universalhaven.Data.Infrastructure;

namespace Service
{
    public class MailService : Service<mail>, IMailService
    {
        private static DatabaseFactory Dbf = new DatabaseFactory();
        private static UnitOfWork utw = new UnitOfWork(Dbf);

        public MailService() : base(utw)
        {

        }
        public IEnumerable<mail> GetMails()
        {
            return GetAll();
        }
     
    }
}