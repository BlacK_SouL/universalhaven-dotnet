﻿using Domain.Models;
using ServicePattern;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Service
{
    public interface ISuggestionService : IService<suggestion>
    {
        IEnumerable<suggestion> GetSuggestionsByUser(long id);
        IEnumerable<suggestion> GetSuggestionsByCamp(long id);
    }
}
