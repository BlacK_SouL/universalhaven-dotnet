﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ServicePattern;
using Domain.Models;
namespace Service
{
    public interface IFundraisingEventService:IService<fundraisingevent>
    {
        IEnumerable<fundraisingevent> GetEventsByUser(long id);
    }
}
