﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Microsoft.AspNet.SignalR;

namespace Web.Hubs
{
    public class TaskCommentHub : Hub
    {
        public void Comment(String taskid, string message, string publisherid, string username)
        {
            Clients.All.addNewMessageToPage(taskid, message, publisherid, username);
        }
    }
}