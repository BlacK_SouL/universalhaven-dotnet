﻿using Domain.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Service;
namespace Web.WebApi
{
    public class MailController : ApiController
    {
        MailService service = new MailService();
        // GET api/<controller>
        public IEnumerable<mail> Get(string keyword="")
        {
            return service.GetMany(m=>m.content.Contains(keyword) || m.subject.Contains(keyword));
        }

        // GET api/<controller>/5
        public string Get(int id)
        {
            return "value";
        }

        // POST api/<controller>
        public mail Post([FromBody]mail value)
        {
            service.Add(value);
            service.Commit();
            return value;
        }

        // PUT api/<controller>/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/<controller>/5
        public void Delete(int id)
        {
        }
    }
}