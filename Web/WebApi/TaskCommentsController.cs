﻿using Domain.Models;
using Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;

namespace Web.WebApi
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class TaskCommentsController : ApiController
    {
        private Object toAnonymous(TaskComment c)
        {
            return new
            {
                TaskCommentId= c.Id,
                PublisherId = c.PublisherId,
                PublishDate = c.PublishDate,
                Content = c.Content,
                TaskId= c.TaskId,
                Publisher = Web.Helpers.UserHelper.GetUserName((long)c.PublisherId),
                Picture = Web.Helpers.UserHelper.GetUserPicPath((long)c.PublisherId)
            };
        }
        ITaskCommentService service = new TaskCommentService();
        // GET api/<controller>
        public ICollection<Object> Get([FromUri] int taskId)
        {
            var res = service.GetMany(c => c.TaskId == taskId);
            var comments = new List<Object>();
            foreach (var com in res)
            {
                comments.Add(toAnonymous(com));
            }
            return comments;
        }

      

        // POST api/<controller>
        public Object Post([FromBody]TaskComment taskComment)
        {
            service.Add(taskComment);
            service.Commit();
            return toAnonymous(taskComment);
        }

        
        // DELETE api/<controller>/5
        public void Delete(int id)
        {
            service.Delete(service.GetById(id));
            service.Commit();
        }
    }
}