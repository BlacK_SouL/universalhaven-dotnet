﻿using Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;

namespace Web.WebApi
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class TaskController : ApiController
    {
        private Object createAnonymousType(Domain.Models.Task task)
        {
            if (task == null)
            {
                return null;
            }
            var t = new
            {
                id = task.TaskId,
                description = task.Description,
                camp = task.campId,
                taskAssigner = Web.Helpers.UserHelper.GetUserName((long)task.TaskAssignerId),
                taskExecutor = Web.Helpers.UserHelper.GetUserName((long)task.TaskExecutorId),
                deadline = task.DeadLine,
                endingDate = task.EndingDate,
                priority = task.priority.ToString(),
                status = task.Status.ToString(),
                startingDate = task.StartingDate,
                commentCount = task.TaskComments.Count()
            };
            return t;
        }
        public ITaskService service = new TaskService();
        public ITaskCommentService serviceComment = new TaskCommentService();
        // GET api/<controller>
        public Object Get([FromUri] long userId=0,[FromUri] string stats="")
        {
            if (stats == "true")
            {
                var res=  service.GetTaskStats(1);
                foreach (var item in res.topthree)
                {
                    item.picture = Web.Helpers.UserHelper.GetUserPicPath(item.Id);
                    item.login = Web.Helpers.UserHelper.GetUserName(item.Id);
                }
                return res;
            }
            if ( (userId!=0) ) {
                var taskss = service.GetMany(t=> t.TaskAssignerId==userId ||t.TaskExecutorId==userId);
                var pending = new List<Object>();
                var doing = new List<Object>();
                var done = new List<Object>();
                var declined = new List<Object>();
                foreach (Domain.Models.Task task in taskss.Where(t=>t.Status==Domain.Models.TaskStatus.PENDING))
                {
                  pending.Add(createAnonymousType(task));
                }
                foreach (Domain.Models.Task task in taskss.Where(t => t.Status == Domain.Models.TaskStatus.DOING))
                {
                    doing.Add(createAnonymousType(task));
                }
                foreach (Domain.Models.Task task in taskss.Where(t => t.Status == Domain.Models.TaskStatus.DONE))
                {
                    done.Add(createAnonymousType(task));
                }
                foreach (Domain.Models.Task task in taskss.Where(t => t.Status == Domain.Models.TaskStatus.DECLINED))
                {
                    declined.Add(createAnonymousType(task));
                }
                return new
                {
                    done = done,
                    doing = doing,
                    declined = declined,
                    pending = pending
                };
            }
            var tasks = service.GetAll();
            var list = new List<Object>();
            foreach (Domain.Models.Task task in tasks)
            {            
               
                list.Add(createAnonymousType(task));
            }
           
            
            return list;
        }

        // GET api/<controller>/5
        public Object Get(int id)
        {
            return createAnonymousType(service.GetById(id));
        }

        // POST api/<controller>
        public Object Post([FromBody] Domain.Models.Task task)
        {
            service.Add(task);
            service.Commit();
            return createAnonymousType(task);
        }

        // PUT api/<controller>/5
        public Object Put(int id, [FromUri] string status)
        {
            var task = service.GetById(id);
            task.Status = (Domain.Models.TaskStatus) Enum.Parse(typeof(Domain.Models.TaskStatus),status,true);
            if (task.Status== Domain.Models.TaskStatus.DONE)
            {
                task.EndingDate = DateTime.Now;
            }
            service.Update(task);
            service.Commit();
            return createAnonymousType(task);
        }

        // DELETE api/<controller>/5
        public void Delete(int id)
        {
            var task = service.GetById(id);
            if (task != null)
            {
                service.Delete(task);
                service.Commit();
            }
        }
    }
}