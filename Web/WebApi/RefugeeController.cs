﻿using Domain.Models;
using Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Web.WebApi
{
    public class RefugeeController : ApiController
    {

        RefugeeService service = new RefugeeService();
        


        // GET api/<controller>
        public IEnumerable<person> Get([FromUri] long campid)
        {
            return service.GetMany(r => r.camp_id==campid);
        }

        // GET api/<controller>/5
        public string Get(int id)
        {
            return "value";
        }

        // POST api/<controller>
        public void Post([FromBody]string value)
        {
        }

        // PUT api/<controller>/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/<controller>/5
        public void Delete(int id)
        {
        }
    }
}