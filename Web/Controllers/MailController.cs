﻿using Domain.Models;
using Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Web.Models;
using Web.RestClients;

namespace Web.Controllers
{
    public class MailController : Controller
    {
        MailRestClient client = new MailRestClient();
        MailService service = new MailService();
    
       
        public ActionResult ContactUs(string message="")
        {
            ViewBag.message = message;
            return View();
        }
        [HttpPost]
        public ActionResult Contactus()
        {
            try
            {
                Mail m = new Mail
                {
                    subject = Request["subject"],
                    content = Request["message"],
                    mailSender = Request["email"],
                };
                client.SendMail(m);
                return RedirectToAction("ContactUs");
            }
            catch
            {
                return RedirectToAction("ContactUs");
            }
        }
        public ActionResult Newsletters(string message = "")
        {
            ViewBag.message = message;
            ViewBag.token = (string)Session["token"];
            ViewBag.getmailshimp = client.getmailchimp();
            return View();
        }
  
      
        // GET: Mail
        public ActionResult Index()
        {
            var mails = service.GetMails();
            ViewBag.token = (string)Session["token"];
            ViewBag.allmails = mails;
            return View(mails);
        }
        public string getmails()
        {

            return " ";
        }
    
        // GET: Mail/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: Mail/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Mail/Create
        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Mail/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: Mail/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Mail/Delete/5
        public ActionResult Delete(int id)
        {
            var mail = service.GetById(id);
            service.Delete(mail);
            service.Commit();
            return RedirectToAction("Index");
        }

        // POST: Mail/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
