﻿using Domain.Models;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web;
using System.Web.Mvc;
using System.Web.SessionState;
using Web.RestClients;

namespace Web.Controllers
{
    [SessionState(SessionStateBehavior.Default)]
    public class HomeController : Controller
    {

        private HttpClient client = new HttpClient();

        public HomeController()
        {
            client.BaseAddress = new Uri("http://127.0.0.1:18080/universalhaven-web/rest/");
            client.DefaultRequestHeaders.Accept.Add(
            new MediaTypeWithQualityHeaderValue("application/json"));
        }

        

        public ActionResult Admin(string message="")
        {
            if (Session["idUser"] == null)
                return RedirectToAction("Login", "Home");
            ViewBag.message = message;
            return View();
        }

        public ActionResult Index()
        {
            return View();
        }

        
        public ActionResult Login()
        {

            if (Session["idUser"] != null)
                return RedirectToAction("Admin");

            string login = this.Request.QueryString["login"];
            string password = this.Request.QueryString["password"];

            if (login != null)
            {
                person p = TokenManager.Authenticate(login, password);
                if (p == null)
                {
                    return View();
                }
                Session["idUser"] = p.id;
                Session["userRole"] = p.role;
                Session["userEmail"] = p.email;
                Session["userName"] = p.name;
                Session["userLastname"] = p.surname;
                Session["userLogin"] = p.login;
                Session["userCamp"] = p.assignedCamp_id;
                Session["password"] = this.Request.QueryString["password"];
                Session["token"] = TokenManager.auth(login, password);

                RestClient client = new RestClient("http://127.0.0.1:18080/universalhaven-web/rest/");
                
                IRestRequest request = new RestRequest("camp/campbyuser", Method.GET);
                request.AddQueryParameter("userid", p.id.ToString());
                IRestResponse response = client.Execute(request);
                var camp = response.Content;
                long campid;
                long.TryParse(camp,out campid);
                Session["campid"] = campid;
                return RedirectToAction("Admin", "Home");

            }




            return View();

        }

        public ActionResult LogOut()
        {

            Session.RemoveAll();
            return RedirectToAction("Index");



        }


        public ActionResult NotFound(){
            return View();
        }

        public ActionResult Chat()
        {
            return View();
        }
    }
}