﻿using Data;
using Domain.Models;
using Service;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using Web.RestClients;

namespace Web.Controllers
{
    public class roomController : Controller
    {
        CampRestClient client = new CampRestClient();
        RoomService roomservice = new RoomService();
       
        public ActionResult Create()
        {
            client.token = (string)Session["token"];
            ViewBag.getallcamp = client.getallcamps();
            return View();
        }

        // POST: room/Create
        [HttpPost]
        public ActionResult CreateRoom()
        {
            client.token = (string)Session["token"];

            room c = new room
            {
               
                Type = (typeroom)Enum.Parse(typeof(typeroom), Request["roomType"], true),
                beds = int.Parse(Request["bed"]),
                superficy = double.Parse(Request["superficy"]),
                reference = Request["ref"],
                campId = long.Parse(Request["campid"]),
                remainingbeds = int.Parse(Request["bed"])

            };
            roomservice.Add(c);
            roomservice.Commit();
                return RedirectToAction("Index", new RouteValueDictionary(
    new { controller = "Camp", action = "Index", message = "success" }));
        }

         }
}
