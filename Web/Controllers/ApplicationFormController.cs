﻿using Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Web.Controllers
{
    public class ApplicationFormController : Controller
    {
        ApplicationFormService applicationDonation = new ApplicationFormService();
        // GET: ApplicationForm
        public ActionResult Index()
        {
            var applications = applicationDonation.GetAll();
            return View(applications);
        }

        // GET: ApplicationForm/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: ApplicationForm/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: ApplicationForm/Create
        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: ApplicationForm/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: ApplicationForm/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: ApplicationForm/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: ApplicationForm/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
