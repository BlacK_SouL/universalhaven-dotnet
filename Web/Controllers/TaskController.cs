﻿using Domain.Models;
using Microsoft.AspNet.SignalR;
using Newtonsoft.Json;
using RestSharp;
using Service;
using ServicePattern;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Web.Hubs;

namespace Web.Controllers
{

    

    
    public class TaskController : Controller
    {
        ITaskService service = new TaskService();
        ITaskCommentService commentService = new TaskCommentService();


        public List<person> getCampStaff()
        {
            RestClient client = new RestClient("http://localhost:18080/universalhaven-web/rest");
            var request = new RestRequest("camp/campstaff", Method.GET);
            long campId = (long)Session["campid"];
            request.AddQueryParameter("campid", campId.ToString());
            IRestResponse<List<person>> response = client.Execute<List<person>>(request);

            return response.Data;
        }
        
        
        // GET: Task
        public ActionResult Index()
        {
           
            var tasks = new TaskLists();
            if (Session["userRole"] == null)
            {
                return RedirectToAction("NotFound", "Home");
            }
            string role = (string)Session["userRole"];
            long id = (long)Session["idUser"];
            var show = Request.QueryString["show"];
            if (  role.Contains("CAMP_MANAGER"))
            {
                
                ViewBag.title = "Assigned tasks";
                long campid = (long)Session["campid"];

                service.GetAssignedTasks(id, campid, role,tasks);
                /* tasks.pending = service.GetMany(task => task.TaskAssignerId == id && task.Status == TaskStatus.PENDING && task.campId==campid);
                 tasks.doing = service.GetMany(task => task.TaskAssignerId == id && task.Status == TaskStatus.DOING && task.campId == campid);
                 tasks.done = service.GetMany(task => task.TaskAssignerId == id && task.Status == TaskStatus.DONE && task.campId == campid);
                 tasks.declined = service.GetMany(task => task.TaskAssignerId == id && task.Status == TaskStatus.DECLINED && task.campId == campid);
                 */
                //ViewBag.tasks = JsonConvert.SerializeObject(tasks);
            }
                
            else if (role.Contains("STAFF"))
            {
                long campid = (long)Session["campid"];
                ViewBag.title = "My tasks";
                service.GetAssignedTasks(id, campid, role, tasks);
                /*tasks.pending = service.GetMany(task => task.TaskExecutorId == id && task.Status == TaskStatus.PENDING && task.campId == campid);
                tasks.doing = service.GetMany(task => task.TaskExecutorId == id && task.Status == TaskStatus.DOING && task.campId == campid);
                tasks.done = service.GetMany(task => task.TaskExecutorId == id && task.Status == TaskStatus.DONE && task.campId == campid);
                tasks.declined = service.GetMany(task => task.TaskExecutorId == id && task.Status == TaskStatus.DECLINED && task.campId == campid);
                */
                var toSerialize = new List<Object>();
                foreach(var item in tasks.doing)
                {
                    toSerialize.Add(new {
                        TaskId = item.TaskId,
                        Description = item.Description,
                        StartDate = item.StartingDate,
                        DeadLine = item.DeadLine
                    });
                }
                foreach (var item in tasks.pending)
                {
                    toSerialize.Add(new
                    {
                        TaskId = item.TaskId,
                        Description = item.Description,
                        StartDate = item.StartingDate,
                        DeadLine = item.DeadLine
                    });
                }
                ViewBag.tasks= JsonConvert.SerializeObject(toSerialize);

            }
            else
            {
                return RedirectToAction("NotFound", "Home");
            }
            return View(tasks);
        }

        // GET: Task/Details/5
        public ActionResult Details(int id)
        {
            if (Session["idUser"] == null)
            {
                return RedirectToAction("NotFound", "Home");
            }
            long userid = (long)Session["idUser"];
            
            Task task = service.GetById(id);
            if (task == null)
            {
                return RedirectToAction("Index");
            }
            if ((task.TaskAssignerId != userid) && (task.TaskExecutorId != userid))
                return RedirectToAction("NotFound", "Home");

            return View(task);
        }

        // GET: Task/Create
        public ActionResult Create()
        {
            string role = (string)Session["userRole"];
            
            if ((role==null) ||(role.Equals("CAMP_MANAGER") == false))
            {
                return RedirectToAction("NotFound", "Home");
            }
            

            return View(getCampStaff().Where(p=>p.role.Equals("CAMP_STAFF")).ToList()); 
        }

        [HttpGet]
        public void ChangeStatus(int taskid,string status)
        {
            service.changeStatus(taskid, status);
            
        }


        // POST: Task/Create
        [HttpPost]
        public ActionResult CreatePost()
        {
            
            long campid = (long)Session["campid"];
            try
            {
                long userid = (long)Session["idUser"];
                var taskk = new Task
                {
                    priority = (TaskPriority) Enum.Parse(typeof (TaskPriority), Request["priority"],true),
                    Status = TaskStatus.PENDING,
                    Description = Request["description"],
                    DeadLine = DateTime.Parse(Request["deadline"]),
                    StartingDate = DateTime.Now,
                    campId = campid,
                    TaskAssignerId = userid,
                    TaskExecutorId = long.Parse(Request["user"])
                };
                service.Add(taskk);
                service.Commit();

                return RedirectToAction("Index");
            }
            catch
            {
                return RedirectToAction("Create");
            }
        }

        // GET: Task/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: Task/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Task/Delete/5
        public ActionResult Delete(int id)
        {
            if (Session["idUser"] == null)
            {
                return RedirectToAction("NotFound", "Home");
            }
            string role = (string) Session["userRole"]; 
            if (role.Equals("CAMP_MANAGER")==false)
            {
                return RedirectToAction("NotFound", "Home");
            }
            var task = service.GetById(id);

            task.camp = null;
            task.campId = null;

            

            service.Delete(task);
            service.Commit();

            return RedirectToAction("Index");
        }


        public ActionResult DeleteComment(int id)
        {
            var comment = commentService.GetById(id);
            Task t = service.GetById((long) comment.TaskId);

            var taskid = comment.TaskId;

            /*comment.TaskId = null;
            comment.Task = null;
            comment.Publisher = null;
            comment.PublisherId = null;
            commentService.Delete(comment);
            commentService.Commit();
            */

            var toDelete = t.TaskComments.Where(c=> c.Id== id).First();

            t.TaskComments.Remove(toDelete);

            service.Update(t);
            service.Commit();

            

            return RedirectToAction("Details/"+ taskid, "Task");
        }
       

        [HttpPost]
        public void AddComment()
        {
            var taskid = long.Parse(Request["taskid"]);
            var userid = ((long)Session["idUser"]);
            var date = DateTime.Now;
            var content = Request["content"];
            var task = service.GetById(taskid);
            
            TaskComment commment = new TaskComment
            {
                TaskId = taskid,
                PublisherId= userid,
                Content= content,
                PublishDate= date
            };
            task.TaskComments.Add(commment);
            //commentService.Add(commment);
            service.Commit();
            
        }

        public ActionResult Stats()
        {
            if (Session["campid"] == null || Session["userRole"] == null)
            {
                return RedirectToAction("NotFound", "Home");
            }
            
            long campid = (long)Session["campid"];
            string role = (string)Session["userRole"];
            if (role.Equals("CAMP_MANAGER") == false)
            {
                return RedirectToAction("NotFound", "Home");
            }

            var stats = service.GetTaskStats(campid);
            
            return View(stats);
        }

    }
}
