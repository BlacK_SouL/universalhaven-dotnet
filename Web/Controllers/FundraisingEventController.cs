﻿using Domain.Models;
using Newtonsoft.Json;
using Service;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web;
using System.Web.Helpers;
using System.Web.Mvc;
using Web.Models;
using Web.RestClients;
using Web.Helpers;
using System.Collections;

namespace Web.Controllers
{
    public class FundraisingEventController : Controller
    {
        FundraisingEventService fs = new FundraisingEventService();
        FundraisingEventRestClient frc = new FundraisingEventRestClient();
        CampService cs = new CampService();
        // GET: FundraisingEvent
        public ActionResult Index()
        {
            if (Session["idUser"] == null)
                return RedirectToAction("Login", "Home");
            ViewBag.eventCountByCountry = frc.eventCountByCountry();
            ViewBag.avgCompletionEvent=frc.avgCompletionEvent();
            var events = fs.GetAll();
            IEnumerable <long> id = events.Select(f => f.id);
            long idd = events.FirstOrDefault().id;
            string val = string.Join("," + Environment.NewLine,
            frc.eventCountByCountry().Select(d => "['" + d.Key + "', " + d.Value + "]"));
            ViewBag.val = val;
            return View(events);
        }
        
        public ActionResult getAllEventsForClient()
        {
            var Events = frc.getAllEvents();
            return View(Events);
        }
        public ActionResult getFinishedEventsForClient()
        {
            var Events = frc.getFinishedEvents();
            return View(Events);
        }
        public ActionResult getInProgressEventsForClient()
        {
            var Events = frc.getEventsInProgress();
            return View(Events);
        }
        public ActionResult getLowStateEvents()
        {
            var Events = frc.getLOWStateEvents();
            return View(Events);
        }
        public ActionResult getMediumStateEvents()
        {
            var Events = frc.getMediumStateEvents();
            return View(Events);
        }
        public ActionResult getHighStateEvents()
        {
            var Events = frc.getHIGHStateEvents();
            return View(Events);
        }
        public ActionResult statsForClient()
        {
            ViewBag.eventCountByCountry = frc.eventCountByCountry();
            ViewBag.avgCompletionEvent = frc.avgCompletionEvent();
            ViewBag.eventCountByMonth = frc.eventCountByMonthThisYear();
            return View();
        }


        public ActionResult getAll()
        {
            if (Session["idUser"] == null)
                return RedirectToAction("Login", "Home");

            var Events = frc.getAllEvents();
            return View(Events);
        }
        
        public ActionResult getByUser()
        {
            if (Session["idUser"] == null)
                return RedirectToAction("Login", "Home");
            IEnumerable<fundraisingevent> f = fs.GetEventsByUser((long)Session["idUser"]);
            return View(f);
        }
        // GET: FundraisingEvent/Details/5
        public ActionResult Details(int id)
        {
            if (Session["idUser"] == null)
                return RedirectToAction("Login", "Home");
            var s = fs.GetById(id);
            ViewBag.sumAmount = frc.getSumAmountDonationByEvent(id);
            return View(s);

        }
        // GET: FundraisingEvent/DetailsEventForClient/5
        public ActionResult DetailsEventForClient(int id)
        {
            var s = fs.GetById(id);
            ViewBag.sumAmount  = frc.getSumAmountDonationByEvent(id);
            /*long avg = ((ViewBag.sumAmount) * 100)/s.goal;
            ViewBag.avg = avg;*/
            return View(s);

        }

        // GET: FundraisingEvent/Create
        public ActionResult Create()
        {
            if(Session["idUser"]==null)
                return RedirectToAction("Login", "Home");
            var list = new List<SelectListItem>
            {
                new SelectListItem {Text = "low", Value = "LOW"},
                new SelectListItem {Text = "medium", Value = "MEDIUM"},
                new SelectListItem {Text = "high", Value = "HIGH"}
            };

            ViewData["list"] = list;
            FundraisingEventModel fm = new FundraisingEventModel();
            fm.camp = cs.GetAll().ToSelectListItem();
            return View(fm);
            
        }

        // POST: FundraisingEvent/Create
        [HttpPost]
        public ActionResult Create(FundraisingEventModel fm, HttpPostedFileBase Image)
        {
            if (!ModelState.IsValid || Image == null || Image.ContentLength == 0)
            {
                RedirectToAction("Create");
            }
            fm.imagePath = Image.FileName;
            try
            {
                frc.addEvent(fm, (string)Session["token"], Convert.ToInt32(Session["idUser"]));
                var path = Path.Combine(Server.MapPath("~/Content/Upload/"), Image.FileName);
                Image.SaveAs(path);
                return RedirectToAction("getByUser");
            }
            catch(NullReferenceException)
            {
                
                return View(fm);
            }
        }

        // GET: FundraisingEvent/Edit/5
        public ActionResult Edit(long id)
        {
            if (Session["idUser"] == null)
                return RedirectToAction("Login", "Home");

            //var s = frc.getEventById(id);
             var s = fs.GetById(id);
            FundraisingEventModel fm = new FundraisingEventModel { };
            fm.id = s.id;
            fm.title = s.title;
            fm.description = s.description;
            fm.camp_id = s.camp_id;
            fm.finishingDate = s.finishingDate;
            fm.state = s.state;
            fm.urgency = s.urgency;
            fm.imagePath = s.imagePath;
            fm.publishDate = s.publishDate;
            fm.publisher_id = s.publisher_id;
            fm.goal = s.goal;
            return View(fm);

        }

        // POST: FundraisingEvent/Edit/5
        [HttpPost]
        public ActionResult Edit(FundraisingEventModel fm)
        {

            try
            {
                frc.editEvent(fm, (string)Session["token"], Convert.ToInt32(Session["idUser"]));
                
                return RedirectToAction("getAll");
            }
            catch //(Exception e)
            {
                return View();
                /*throw;
                return RedirectToAction("Edit");*/
            }
        }

        // GET: FundraisingEvent/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: FundraisingEvent/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
