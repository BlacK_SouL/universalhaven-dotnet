﻿using Domain.Models;
using Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using Web.Models;
using Web.RestClients;

namespace Web.Controllers
{
    public class CampController : Controller
    {
        CampRestClient client = new CampRestClient();
        RefugeeService servicerefugee = new RefugeeService();
        RoomService serviceroom = new RoomService();
        // GET: Camp
        public ActionResult Index(string message="")
        {

            ViewBag.token = client.token = (string)Session["token"];
            ViewBag.message = message;
            ViewBag.males = servicerefugee.GetRefugeePer("male");
            ViewBag.females = servicerefugee.GetRefugeePer("female");
            ViewBag.totalroom = serviceroom.GetAll().Count(); 
            ViewBag.Couple = serviceroom.GetRoomperType("Couple").Count();
            ViewBag.Male = serviceroom.GetRoomperType("Male").Count();
            ViewBag.Female = serviceroom.GetRoomperType("Female").Count();
            ViewBag.Families = serviceroom.GetRoomperType("Families").Count();
            ViewBag.total = servicerefugee.GetRefugeePer("female") + servicerefugee.GetRefugeePer("male") ;
            ViewBag.getcamppercoutry = client.getallcampspercountry();
            ViewBag.countcamppercoutry = client.countAllCampsPerCountry();
            return View();
        }

        // GET: Camp/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: Camp/Create
        public ActionResult Create()
        {
            client.token = (string)Session["token"];

            ViewBag.getcampmanager = client.FindCampMan();
            return View();
        }

        // POST: Camp/Create
        [HttpPost]
        public ActionResult CreateCamp()
        {
            client.token = (string)Session["token"];
            var electricity = false;
            var water = false;
            try
            {
                
                if (Request["electricity"] != null)
                {
                    electricity = true;
                }
                if (Request["water"] != null)
                {
                    water = true;
                }
                Camp c = new Camp
                {
                    
                    name = Request["campname"],
                    address = Request["address"],
                    superficy = double.Parse(Request["superficy"]),
                    capacity = int.Parse(Request["capacity"]),
                    occupancy = int.Parse(Request["occupancy"]),
                    budget = double.Parse(Request["budget"]),
                    campCreator = (long) Session["idUser"],
                    campManager = long.Parse(Request["campManager"]),
                    country = Request["country"],
                    electricity = electricity,
                    water = water,
                    latLng = Request["latlng"]

                };

                 client.AddCamp(c);
                return RedirectToAction("Index", new RouteValueDictionary(
   new { controller = "Camp", action = "Index", message = "success" }));

            }
            catch
            {
                return View();
            }
        }
        public ActionResult UpdateCamp(long id)
        {
            client.token = (string)Session["token"];
            ViewBag.idcamp = id;
            ViewBag.getcamp = client.getCampById(id);
            ViewBag.getcampmanager = client.FindCampMan();
            return View();
        }
       

        [HttpPost]
        public string UpdateCamp()
        {

            client.token = (string)Session["token"];
            var electricity = false;
            var water = false;
            try
            {

                if (Request["electricity"] != null)
                {
                    electricity = true;
                }
                if (Request["water"] != null)
                {
                    water = true;
                }
                Camp c = new Camp
                {
                    id = long.Parse(Request["campid"]),
                    name = Request["campname"],
                    address = Request["address"],
                    superficy = double.Parse(Request["superficy"]),
                    capacity = int.Parse(Request["capacity"]),
                    occupancy = int.Parse(Request["occupancy"]),
                    budget = double.Parse(Request["budget"]),
                    campCreator = (long)Session["idUser"],
                    campManager = long.Parse(Request["campManager"]),
                    country = Request["country"],
                    electricity = electricity,
                    water = water,
                    latLng = Request["latlng"]

                };

                return client.updatecamp(c);

            }
            catch
            {
                return "Error";
            }
        }




        // GET: Camp/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: Camp/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Camp/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: Camp/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
