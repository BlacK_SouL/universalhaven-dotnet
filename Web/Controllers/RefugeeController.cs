﻿using Data;
using Domain.Models;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Service;
using ServicePattern;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using Web.RestClients;

namespace Web.Controllers
{
    public class RefugeeController : Controller
    {
        CampRestClient client = new CampRestClient();
        person refugee = new person();

        RefugeeService service = new RefugeeService();
        RoomService serviceRoom = new RoomService();
        CampService serviceCamp = new CampService();

        public ActionResult Index(string message = "")
        {

            ViewBag.refugees = client.getallrefugees();
            ViewBag.message = message;
            return View();
        }
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: Refugee/Create
        public ActionResult Create()
        {
            return View();
        }
      
        [HttpPost]
        public string CreateRefugees()
        {
            client.token = (string)Session["token"];
            int idroom = 0;
            long CampId = 0;
            int familynbr = 0;
            // long test = (long)Session["userCamp"];
            if (Request.ContentType.Contains("json"))
            {
                System.IO.StreamReader sr = new System.IO.StreamReader(Request.InputStream);
                string line = "";
                line = sr.ReadToEnd();
                dynamic dynJson = JsonConvert.DeserializeObject(line.ToString());
                foreach (var i in dynJson)
                {
                    if (i.roomId != null)
                        idroom = i.roomId;
                    if (i.CampId != null)
                        CampId = i.CampId;
                    if (i.familynbr != null)
                        familynbr = i.familynbr;

                }
                System.Diagnostics.Debug.WriteLine("this is the camp id" + CampId);
                foreach (var item in dynJson)
                {
                    if (item.maritalStatus == null)
                    {
                        person p = new person
                        {
                            type = "refugee",
                            birthDate = item.birthDate,
                            gender = item.gender,
                            name = item.name,
                            surname = item.surname,
                            country = item.country,
                            identityNumber = item.identityNumber,
                            identityType = 0,
                            maritalStatus = "single",
                            specialNeeds = item.specialNeeds,
                            roomId = idroom,
                            camp_id = CampId

                        };
                        service.Add(p);
                        service.Commit();
                    }
                    if (item.maritalStatus != null)
                    {
                        person pp = new person
                        {
                            birthDate = (long) item.birthDate,
                            gender = item.gender,
                            name = item.name,
                            surname = item.surname,
                            identityNumber = item.identityNumber,
                            specialNeeds = item.specialNeeds,
                            roomId = idroom,
                            type = "refugee",
                            country = item.country,
                            emergencyContact = item.country,
                            identityType = (int) item.identityType ,
                            maritalStatus = item.maritalStatus,
                            profession = item.profession,
                            skills = item.skills,
                            picture = item.picture,
                            camp_id = CampId
                        };
                        service.Add(pp);
                        service.Commit();
                        
                    }
                }
                serviceRoom.updateBeds(idroom, familynbr);
                serviceCamp.updateCampscapacity(CampId, familynbr);
                return line;
            }

            return "error";

        }


        [HttpPost]
        public string findRoom()
        {
            string json="";     
            var familynbr = int.Parse(Request["familynbr"]);
            var typebed = Request["Type"];

            /*var chambres = (from p in db.rooms
                            where p.remainingbeds > familynbr
                            where p.Type == typebed
                            select p).ToList();*/

            var chambres = serviceRoom.GetAvailableRooms(familynbr, typebed);
            
            foreach(var item in chambres)
            {
                Console.WriteLine(item.roomId);
                json = JsonConvert.SerializeObject(chambres);
            }
            return json;
        }
        
        // GET: Refugee/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: Refugee/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Refugee/Delete/5
        public ActionResult Delete(int id)
        {
            service.Delete(service.GetById(id));
            service.Commit();
            return View();
        }

    }
}
