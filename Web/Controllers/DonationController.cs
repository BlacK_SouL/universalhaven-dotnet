﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Service;
using Web.RestClients;
using Web.Models;

namespace Web.Controllers
{
    public class DonationController : Controller
    {
        DonationService serviceDonation = new DonationService();
        DonationRestClient client = new DonationRestClient();
        // GET: Donation
        public ActionResult Index()
        {
            if ( (Session["userRole"]==null) || (((string)Session["userRole"]).Contains("ICRC_MANAGER") == false) )
                return RedirectToAction("NotFound","Home");
            client.token = (string) Session["token"];
            ViewBag.bycountry = client.DonationsPerCountry();
            ViewBag.bycountryaverage = client.AverageDonationsPerCountry();
            var overall = client.DonationsByDateOverall();
            ViewBag.overall = overall;
            ViewBag.perday = client.DonationsByDay();
            ViewBag.permethod = client.DonationsPerPaymentMethod();
            ViewBag.average = client.AverageDonatedAmount();

            return View();

        }
        [HttpPost]
        public ActionResult CreatePost()       
        {
            DonationParam param = new DonationParam();
            if (Request["regular"].Contains("once"))
            {
                param.donation = new Donation
                {
                    amount = Double.Parse(Request["amount"]),
                    contributorName = Request["firstName"] + " " + Request["lastName"],
                    contributorEmail = Request["email"],
                    contributorAddress = Request["address"],
                    country = Request["country"]
                };
                param.method = "stripe";
                param.token = Request["token"];
                
                try
                {
                    client.Donate(param);
                    ViewBag.error = false;
                    ViewBag.message = "Your donation was successfuly transfered, thank you !  ";
                }
                catch (Exception e)
                {
                    ViewBag.error = true;
                    ViewBag.message = "An error occured, please try again later..";
                }
                
                
                return View();
            }
            else
            {
                try
                {
                    client.Subscribe(Request["email"], Request["plan"], Request["token"], Request["firstname"] + " " + Request["lastname"]);
                    ViewBag.error = false;
                    ViewBag.message = "Your subscription was successfuly created, thank you for  trusting us !  ";
                }
                catch (Exception e)
                {
                    ViewBag.message = "An error occured, please try again later..";
                }
                return View();


            }
        }

        [HttpGet]
        public ActionResult Create()
        {
            return View();
        }


       
    }
}
