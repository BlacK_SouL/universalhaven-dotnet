﻿using Domain.Models;
using Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Web.Helpers;
using Web.Models;

namespace Web.Controllers
{
    public class SuggestionController : Controller
    {
        SuggestionService ss = new SuggestionService();
        CampService cs = new CampService();
        PersonService ps = new PersonService();
        // GET: Suggestion
        public ActionResult Index()
        {
            if (Session["idUser"] == null)
                return RedirectToAction("Login", "Home");
            var s = ss.GetAll();
            return View(s);
        }
        public ActionResult getByUser()
        {
            if (Session["idUser"] == null)
                return RedirectToAction("Login", "Home");
            IEnumerable<suggestion> s = ss.GetSuggestionsByUser((long)Session["idUser"]);
            return View(s);
        }

        // GET: Suggestion/Details/5
        public ActionResult Details(int id)
        {
            if (Session["idUser"] == null)
                return RedirectToAction("Login", "Home");
            var s = ss.GetById(id);
            return View(s);
        }

        // GET: Suggestion/Create
        public ActionResult Create()
        {
            if (Session["idUser"] == null)
                return RedirectToAction("Login", "Home");
            SuggestionModel sm = new SuggestionModel();
            sm.camp = cs.GetAll().ToSelectListItem();
            return View(sm);
        }

        // POST: Suggestion/Create
        [HttpPost]
        public ActionResult Create(SuggestionModel sm)
        {
            try
            {
                suggestion s = new suggestion
                {
                    title=sm.title,
                    description=sm.description,
                    camp_id=sm.camp_id,
                    publisher_id= (long)Session["idUser"]

                };
                ss.Add(s);
                ss.Commit();

                // TODO: Add insert logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Suggestion/Edit/5
        public ActionResult Edit(int id)
        {
            if (Session["idUser"] == null)
                return RedirectToAction("Login", "Home");
            var s = ss.GetById(id);
            return View(s);
        }

        // POST: Suggestion/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, SuggestionModel sm)
        {
            try
            {
                suggestion s = ss.GetById(id);
               /* suggestion s1 = new suggestion { };
                s1 = ss.GetById(id);
                s1.title = "aaa";
                s1.description = "bb";*/
                s.id = sm.id;
                s.title = sm.title;
                s.description = sm.description;
                s.camp_id = sm.camp_id;
                s.publisher_id = sm.publisher_id;
                /*s.person = ps.GetById((long)sm.publisher_id);
                s.camp = cs.GetById((long)sm.camp_id);*/
                //var s = ss.GetById(id);
                ss.Update(s);
                ss.Commit();
                return RedirectToAction("getByUser");
            }
            catch
            {
                return View();
            }
        }

        // GET: Suggestion/Delete/5
        public ActionResult Delete(int id)
        {
            if (Session["idUser"] == null)
                return RedirectToAction("Login", "Home");
            var s = ss.GetById(id);
            return View(s);
        }

        // POST: Suggestion/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, SuggestionModel sm)
        {
            try
            {
                var s = ss.GetById(id);

                ss.Delete(s);
                ss.Commit();

                // TODO: Add delete logic here

                return RedirectToAction("getByUser");
            }
            catch
            {
                return View();
            }
        }
    }
}
