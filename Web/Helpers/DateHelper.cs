﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Web.Helpers
{
    public static class DateHelper
    {
        public static string HumanReadableDate(DateTime date)
        {
            var now = DateTime.Now;
            var diff = now - date;
            
            if (diff.Days != 0)
            {
                return diff.Days + " days ago";
            }
            if (diff.Hours != 0)
            {
                return diff.Hours + " hours ago";
            }
            if (diff.Minutes != 0)
            {
                return diff.Minutes + " minutes ago";
            }
            return diff.Seconds + " seconds ago";
                     
        }

        public static String Now()
        {
            DateTime now = DateTime.Now;

            return now.Day + "/" + now.Month + "/" + now.Year;
        }
    }
}