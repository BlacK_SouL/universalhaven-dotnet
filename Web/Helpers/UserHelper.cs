﻿using Domain.Models;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Web.Helpers
{
    public static class UserHelper
    {
        public static List<person> users;
        public static string GetUserName(long id)
        {
            if (users == null)
            {
                RestClient client = new RestClient("http://localhost:18080/universalhaven-web/rest");
                var request = new RestRequest("user/all", Method.GET);
                
                IRestResponse<List<person>> response = client.Execute<List<person>>(request);

                users = response.Data;
            }
            if (users.Find(u => u.id == id) == null)
            {
                users = null;
                return GetUserName(id);
            }
            return users.Find(u=>u.id==id).login;
        }

        public static string GetUserPicPath(long id)
        {
            if (users == null)
            {
                RestClient client = new RestClient("http://localhost:18080/universalhaven-web/rest");
                var request = new RestRequest("user/all", Method.GET);

                IRestResponse<List<person>> response = client.Execute<List<person>>(request);

                users = response.Data;
            }
            var us = (users.Find(u => u.id == id));
            if ((us.picture==null) || (us.picture.Equals("") == true))
            {
                return "/Content/Images/profile.png";
            }
            else
            {
                return "/Content/Uploads/User/" + us.picture;
            }
        }
    }
}