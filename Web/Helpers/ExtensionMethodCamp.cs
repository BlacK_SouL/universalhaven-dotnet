﻿using Domain.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Web.Helpers
{
    public static class ExtensionMethodCamp
    {
        public static IEnumerable<SelectListItem> ToSelectListItem(this IEnumerable<camp> camps)
        {
            return camps.Select(c => new SelectListItem {
                Text = c.country+" :"+c.address,
                Value = c.id.ToString()
                });
        }
    }
}