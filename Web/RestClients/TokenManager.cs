﻿using Domain.Models;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Web.RestClients
{
    
    public class TokenManager 
    {
        
        static string baseUri = "http://localhost:18080/universalhaven-web/rest";

        public static person Authenticate(string login, string password)
        {
            RestClient restclient = new RestClient("http://127.0.0.1:18080/universalhaven-web/rest/");
            var request = new RestRequest("user/login", Method.POST);
            request.AddParameter("login", login);
            request.AddParameter("password", password);
            IRestResponse<person> response = restclient.Execute<person>(request);
            var user = response.Data;
            return user;
        }


        public static string auth(string username,string password)
        {
            var client = new RestClient(baseUri);
            var request = new RestRequest("auth", Method.POST);
            request.AddParameter("login", username);
            request.AddParameter("password", password);
            IRestResponse response = client.Execute(request);
            var content = response.Content;
            return content;

        }


    }
}