﻿using Domain.Models;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Web.Models;

namespace Web.RestClients
{

    public class CampRestClient
    {
        
        static string baseUri = "http://localhost:18080/universalhaven-web/rest";
        public string token = "";
        


        public string getToken()
        {    
            return token;
        }
    


        public string AddCamp(Camp param) {
            var client = new RestClient(baseUri);

            var request = new RestRequest("camp", Method.POST);
            request.AddHeader("AUTHORIZATION", getToken());
            request.AddJsonBody(param);

            IRestResponse response = client.Execute(request);
            var content = response.Content;

            return response.StatusDescription;
            

        }
     

        public string FindCampMan()
        {
            var client = new RestClient(baseUri);
            var request = new RestRequest("camp/findmanager", Method.GET);
            //request.AddHeader("AUTHORIZATION", getToken());
            IRestResponse response = client.Execute(request);
            var content = response.Content;
            return response.Content;
        }
        public string getallcamps()
        {
            var client = new RestClient(baseUri);
            var request = new RestRequest("camp/findallcamps", Method.GET);
            request.AddHeader("AUTHORIZATION", getToken());
            IRestResponse response = client.Execute(request);
            var content = response.Content;
            return response.Content;
        }
        public string getallrefugees()
        {
            var client = new RestClient(baseUri);
            var request = new RestRequest("camp/findallrefugees", Method.GET);
           // request.AddHeader("AUTHORIZATION", getToken());
            IRestResponse response = client.Execute(request);
            var content = response.Content;
            return response.Content;
        }
        public string countAllCampsPerCountry()
        {
            var client = new RestClient(baseUri);
            var request = new RestRequest("camp/countcamps", Method.GET);
            request.AddHeader("AUTHORIZATION", getToken());
            IRestResponse response = client.Execute(request);
            var content = response.Content;
            return response.Content;
        }
        public string disbandcamp(long campId)
        {
            var client = new RestClient(baseUri);
            var request = new RestRequest("camp/disbandcamps", Method.PUT);
            request.AddHeader("AUTHORIZATION", getToken());
            request.AddParameter("id", campId);
            IRestResponse response = client.Execute(request);
            var content = response.Content;
            return response.Content;
        }
        public string updatecamp(Camp param)
        {
            var client = new RestClient(baseUri);
            var request = new RestRequest("camp", Method.PUT);
            request.AddHeader("AUTHORIZATION", getToken());
            request.AddJsonBody(param);
            IRestResponse response = client.Execute(request);
            var content = response.Content;
            return response.Content;
        }
        public string getallcampspercountry()
        {
            var client = new RestClient(baseUri);
            var request = new RestRequest("camp/findbycountry", Method.GET);
            //request.AddHeader("AUTHORIZATION", getToken());
            IRestResponse response = client.Execute(request);
            var content = response.Content;
            return response.Content;
        }
        public string getCampById(long campId)
        {
            var client = new RestClient(baseUri);
            var request = new RestRequest("camp/findCampById", Method.GET);
            request.AddHeader("AUTHORIZATION", getToken());
            request.AddParameter("id", campId);
            IRestResponse response = client.Execute(request);
            var content = response.Content;
            return response.Content;
        }
    }
}