﻿
    
    using Domain.Models;
using RestSharp;
using Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Web.Models;

namespace Web.RestClients
{

     public class MailRestClient
    {

        static string baseUri = "http://localhost:18080/universalhaven-web/rest";
        public string token = "";
        public string tokenmalchimp = "apikey b63efc6268ec880ad761d95629c320ca-us17";
        static string mailchimpuri = "https://us17.api.mailchimp.com/3.0/lists/7bead1c99a/";
        MailService service = new MailService();
        public string getToken()
        {
            return token;
        }


        public string getmailchimp()
        {
            var client = new RestClient(mailchimpuri);

            var request = new RestRequest("members", Method.GET);
            request.AddHeader("AUTHORIZATION", tokenmalchimp);

            IRestResponse response = client.Execute(request);
            var content = response.Content;

            return response.Content;


        }
        public string SendMail(Mail param)
        {
            var client = new RestClient(baseUri);

            var request = new RestRequest("mail", Method.POST);
            request.AddJsonBody(param);
            IRestResponse response = client.Execute(request);
            var content = response.Content;
            return response.StatusDescription;


        }
        public string sendMailPer(Mail param)
        {
            var client = new RestClient(baseUri);

            var request = new RestRequest("mail/sendmail", Method.POST);
            request.AddJsonBody(param);
            IRestResponse response = client.Execute(request);
            var content = response.Content;
            return response.StatusDescription;


        }
        public string SendNewsLetter(Mail param)
        {
            var client = new RestClient(baseUri);

            var request = new RestRequest("mail/newsletter", Method.POST);
            request.AddHeader("AUTHORIZATION", getToken());
            request.AddJsonBody(param);
            IRestResponse response = client.Execute(request);
            var content = response.Content;
            return response.StatusDescription;


        }
        public string DeleteMail(Mail param)
        {
            var client = new RestClient(baseUri);
            var request = new RestRequest("mail", Method.DELETE);
            request.AddHeader("AUTHORIZATION", getToken());
            request.AddJsonBody(param);
            IRestResponse response = client.Execute(request);
            var content = response.Content;
            return response.StatusDescription;


        }
        public string getsubs(Mail param)
        {
            
            return "";


        }
    }
}