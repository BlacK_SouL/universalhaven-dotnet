﻿using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Web.Models;

namespace Web.RestClients
{
    public class ByDateOverall
    {
        public double today { get; set; }
        public double thismonth { get; set; }
        public double thisyear { get; set; }
    }
    public class DonationRestClient
    {
        
        static string baseUri = "http://localhost:18080/universalhaven-web/rest";
        public string token = "";
        
        
        public string getToken()
        {    
            return token;
        }
        public string DonationsPerCountry()
        {
            var client = new RestClient(baseUri);

            var request = new RestRequest("donation/bycountry", Method.GET);
            request.AddHeader("AUTHORIZATION", getToken());
           
            IRestResponse response = client.Execute(request);
            var content = response.Content;

            return content;
        }
        public string AverageDonationsPerCountry()
        {
            var client = new RestClient(baseUri);

            var request = new RestRequest("donation/bycountry/average", Method.GET);
            request.AddHeader("AUTHORIZATION", getToken());
            
            IRestResponse response = client.Execute(request);
            var content = response.Content;

            return content;
        }

        public ByDateOverall DonationsByDateOverall()
        {
            var client = new RestClient(baseUri);

            var request = new RestRequest("donation/bydate/overall", Method.GET);
            request.AddHeader("AUTHORIZATION", getToken());
            
            IRestResponse<ByDateOverall> response = client.Execute<ByDateOverall>(request);
            
            
           
            return response.Data;
        }
        public String Subscribe(string email, string plan, string token, string name) 
        {
            var client = new RestClient(baseUri);

            var request = new RestRequest("subscription", Method.POST);
            request.AddParameter("email", email);
            request.AddParameter("plan", plan);
            request.AddParameter("token", token);
            request.AddParameter("name", name);

            var response = client.Execute(request);
            if (response.StatusCode.ToString().Contains("OK")==false) 
            {
                throw new Exception();
            }
            return response.Content;
        }
        public string DonationsByDay()
        {
            var client = new RestClient(baseUri);

            var request = new RestRequest("donation/bydate", Method.GET);
            request.AddHeader("AUTHORIZATION", getToken());

            IRestResponse response = client.Execute(request);
            var content = response.Content;

            return content;
        }

        public String DonationsPerPaymentMethod()
        {
            var client = new RestClient(baseUri);

            var request = new RestRequest("donation/bymethod", Method.GET);
            request.AddHeader("AUTHORIZATION", getToken());

            IRestResponse response = client.Execute(request);
            var content = response.Content;

            return content;
        }
        public string AverageDonatedAmount()
        {
            var client = new RestClient(baseUri);

            var request = new RestRequest("donation/average", Method.GET);
            request.AddHeader("AUTHORIZATION", getToken());

            IRestResponse response = client.Execute(request);
            var content = response.Content;

            return content;
        }


        public string Donate(DonationParam param) {
            var client = new RestClient(baseUri);

            var request = new RestRequest("donation", Method.POST);
            request.AddJsonBody(param);

            IRestResponse response = client.Execute(request);
            var content = response.Content;
            if (response.StatusCode.ToString().Contains("OK") == false)
            {
                throw new Exception();
            }
            return content;

        }
    }
}