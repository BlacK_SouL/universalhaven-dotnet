﻿using Domain.Models;
using RestSharp;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web;
using Web.Models;

namespace Web.RestClients
{

    public class FundraisingEventRestClient
    {

        
        static string baseUri = "http://localhost:18080/universalhaven-web/rest";
        static string token = "";
        public IEnumerable<fundraisingevent> getAllEvents()
        {
            HttpClient client = new HttpClient();
            client.DefaultRequestHeaders.Accept.Add(
            new MediaTypeWithQualityHeaderValue("application/json"));
            var response = client.GetAsync("http://localhost:18080/universalhaven-web/rest/fundraisingEvent").
                Result;
            var Events = response.Content.ReadAsAsync<IEnumerable<fundraisingevent>>().Result;
            return Events;

        }
        public IEnumerable<fundraisingevent> getEventsByUser(string idUser)
        {
            HttpClient client = new HttpClient();
            client.DefaultRequestHeaders.Accept.Add(
            new MediaTypeWithQualityHeaderValue("application/json"));
            var response = client.GetAsync("http://localhost:18080/universalhaven-web/rest/fundraisingEvent/event??idUser="+idUser).
                Result;
            var Events = response.Content.ReadAsAsync<IEnumerable<fundraisingevent>>().Result;
            return Events;

        }
        public IEnumerable<fundraisingevent> getEventsInProgress()
        {
            HttpClient client = new HttpClient();
            client.DefaultRequestHeaders.Accept.Add(
            new MediaTypeWithQualityHeaderValue("application/json"));
            var response = client.GetAsync("http://localhost:18080/universalhaven-web/rest/fundraisingEvent/event?state=In progress").
                Result;
            var Events = response.Content.ReadAsAsync<IEnumerable<fundraisingevent>>().Result;
            return Events;

        }
        public IEnumerable<fundraisingevent> getFinishedEvents()
        {
            HttpClient client = new HttpClient();
            client.DefaultRequestHeaders.Accept.Add(
            new MediaTypeWithQualityHeaderValue("application/json"));
            var response = client.GetAsync("http://localhost:18080/universalhaven-web/rest/fundraisingEvent/event?state=Finished").
                Result;
            var Events = response.Content.ReadAsAsync<IEnumerable<fundraisingevent>>().Result;
            return Events;

        }
        public IEnumerable<fundraisingevent> getLOWStateEvents()
        {
            HttpClient client = new HttpClient();
            client.DefaultRequestHeaders.Accept.Add(
            new MediaTypeWithQualityHeaderValue("application/json"));
            var response = client.GetAsync("http://localhost:18080/universalhaven-web/rest/fundraisingEvent/event?urgency=LOW").
                Result;
            var Events = response.Content.ReadAsAsync<IEnumerable<fundraisingevent>>().Result;
            return Events;

        }
        public IEnumerable<fundraisingevent> getMediumStateEvents()
        {
            HttpClient client = new HttpClient();
            client.DefaultRequestHeaders.Accept.Add(
            new MediaTypeWithQualityHeaderValue("application/json"));
            var response = client.GetAsync("http://localhost:18080/universalhaven-web/rest/fundraisingEvent/event?urgency=MEDIUM").
                Result;
            var Events = response.Content.ReadAsAsync<IEnumerable<fundraisingevent>>().Result;
            return Events;

        }
        public IEnumerable<fundraisingevent> getHIGHStateEvents()
        {
            HttpClient client = new HttpClient();
            client.DefaultRequestHeaders.Accept.Add(
            new MediaTypeWithQualityHeaderValue("application/json"));
            var response = client.GetAsync("http://localhost:18080/universalhaven-web/rest/fundraisingEvent/event?urgency=HIGH").
                Result;
            var Events = response.Content.ReadAsAsync<IEnumerable<fundraisingevent>>().Result;
            return Events;

        }
        public void addEvent(FundraisingEventModel fm, string token, int idUser)
        {
            var httpWebRequest = (HttpWebRequest)WebRequest.Create("http://localhost:18080/universalhaven-web/rest/fundraisingEvent");
            httpWebRequest.ContentType = "application/json";
            httpWebRequest.Method = "POST";
            httpWebRequest.Headers.Add("AUTHORIZATION", token);

            using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
            {
                streamWriter.Write(@"{""title"":""" + fm.title + @""",""description"":""" + fm.description + @""",
                    ""urgency"":""" + fm.urgency + @""",""goal"":""" + fm.goal + @""",""imagePath"":""" + fm.imagePath + @""",""publisher"":{""id"":""" + idUser+ @"""},""camp"":{""id"":""" + fm.camp_id + @"""}
                    }");
                streamWriter.Flush();
                streamWriter.Close();
            }

            var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
            using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
            {
                var result = streamReader.ReadToEnd();
            }
        }
        public void editEvent(FundraisingEventModel fm,string token,int idUser)
        {
            var httpWebRequest = (HttpWebRequest)WebRequest.Create("http://localhost:18080/universalhaven-web/rest/fundraisingEvent");
            httpWebRequest.ContentType = "application/json";
            httpWebRequest.Method = "PUT";
            httpWebRequest.Headers.Add("AUTHORIZATION", token);

            using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
            {
                streamWriter.Write(@"{""id"":""" + fm.id + @""",""title"":""" + fm.title + @""",""description"":""" + fm.description + @""",
                    ""urgency"":""" + fm.urgency + @""",""goal"":""" + fm.goal + @""",""state"":""" + fm.state + @""",""imagePath"":""" + fm.imagePath + @""",""publisher"":{""id"":""" +idUser + @"""},""camp"":{""id"":""" + fm.camp_id + @"""}
                    }");
                streamWriter.Flush();
                streamWriter.Close();
            }

            var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
            using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
            {
                var result = streamReader.ReadToEnd();
            }
        }
        
        public Dictionary<string,long> eventCountByCountry()
        {
            var client = new RestClient(baseUri);

            var request = new RestRequest("fundraisingEvent/eventByCountry", Method.GET);
            IRestResponse<Dictionary<string,long>> response = client.Execute<Dictionary<string, long>>(request);
            var content = response.Data;
            return content;
        }
        public Dictionary<string, long> eventCountByMonthThisYear()
        {
            var client = new RestClient(baseUri);

            var request = new RestRequest("fundraisingEvent/event?year=2017", Method.GET);
            IRestResponse<Dictionary<string, long>> response = client.Execute<Dictionary<string, long>>(request);
            var content = response.Data;
            return content;
        }
        public Dictionary<string, long> avgCompletionEvent()
        {
            var client = new RestClient(baseUri);

            var request = new RestRequest("fundraisingEvent/avgCompletionEvent", Method.GET);
            IRestResponse<Dictionary<string, long>> response = client.Execute<Dictionary<string, long>>(request);
            var content = response.Data;
            return content;
        }
        public Dictionary<string, long> eventThisYearForEachMonth()
        {
            var client = new RestClient(baseUri);

            var request = new RestRequest("fundraisingEvent/event?year=2017", Method.GET);
            IRestResponse<Dictionary<string, long>> response = client.Execute<Dictionary<string, long>>(request);
            var content = response.Data;
            return content;
        }
        public fundraisingevent getEventById(long id)
        {
            var client = new RestClient(baseUri);

            var request = new RestRequest("fundraisingEvent/event/?idEvent="+id, Method.GET);
            IRestResponse<fundraisingevent> response = client.Execute<fundraisingevent>(request);
            var content = response.Data;
            return content;

        }
        public string getSumAmountDonationByEvent(long id)
        {
            var client = new RestClient(baseUri);

            var request = new RestRequest("fundraisingEvent/event?idFundraisingEvent="+id, Method.GET);

            IRestResponse response = client.Execute(request);
            var content = response.Content;

            return content;
        }

    }
}