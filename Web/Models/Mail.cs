﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Web.Models
{
    public enum UserRole
    {
        SUPER_ADMIN,
        ICRC_MANAGER,
        CAMP_MANAGER,
        CAMP_STAFF,
        LOGISTICS_AND_REFUGEES_MANAGER,
        VOLUNTEER
    }
    public class Mail
    {
        public int id { get; set; }
        public string subject { get; set; }
        [AllowHtml]
        public string content { get; set; }
        public string mailSender { get; set; }

    }
}