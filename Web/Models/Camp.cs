﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Web.Models
{
    public class Camp
    {
        public long id { get; set; }
        public string name { get; set; }

        public string latLng { get; set; }

        public string address { get; set; }

        public double superficy { get; set; }

        public Boolean electricity { get; set; }

        public Boolean water { get; set; }

        public int capacity { get; set; }

        public int  occupancy { get; set; }

        public double budget { get; set; }

        public string country { get; set; }

        public long campManager { get; set; }

        public long campCreator { get; set; }
    }
}