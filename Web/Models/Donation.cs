﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Web.Models
{
    public class Donation
    {
        public string contributorName { get; set; }
        public string contributorEmail { get; set; }
        public string contributorAddress { get; set; }
        public string country { get; set; }
        public double amount { get; set; }  
    }
}