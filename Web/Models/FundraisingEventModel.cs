﻿using Domain.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Web.Models
{
    public class FundraisingEventModel
    {
        public long id { get; set; }
        [Required]
        public string description { get; set; }
        public Nullable<System.DateTime> finishingDate { get; set; }
        [Required]
        public double goal { get; set; }
        [Required]
        public string imagePath { get; set; }
        public Nullable<System.DateTime> publishDate { get; set; }
        [Required]
        public string state { get; set; }
        [Required]
        public string title { get; set; }
        [Required]
        public string urgency { get; set; }
        public Nullable<long> camp_id { get; set; }
        public Nullable<long> publisher_id { get; set; }
        //public virtual camp camp { get; set; }
        //public virtual user user { get; set; }
        public IEnumerable<SelectListItem> camp { get; set; }
        //public List<SelectListItem> states { get; set; }



    }
}