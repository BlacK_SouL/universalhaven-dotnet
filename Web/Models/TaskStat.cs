﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Web.Models
{
    public class TaskStat
    {
        public int pending;
        public int doing;
        public int done;
        public int declined;
        public int ontime;
        public int afterdeadline;
        public int total;
    }
}