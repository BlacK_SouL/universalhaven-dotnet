﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Web.Models
{
    public class DonationParam
    {
        public Donation donation { get; set; }
        public string method { get; set; }
        public string token { get; set; }
        public string creditCardType { get; set; }
        public string creditCardNumber { get; set; }
        public int expireMonth { get; set; }
        public int expireYear { get; set; }
        public string cvv2 { get; set; }
    }

}