﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Web.Models
{
    public class TaskLists
    {
        public IEnumerable<Domain.Models.Task> pending { get; set; }
        public IEnumerable<Domain.Models.Task> doing { get; set; }
        public IEnumerable<Domain.Models.Task> done { get; set; }
        public IEnumerable<Domain.Models.Task> declined { get; set; }

    }
}