﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Web.Models
{
    public class SuggestionModel
    {
        public long id { get; set; }
        [Required]
        public string description { get; set; }
        [Required]
        public string title { get; set; }
        public Nullable<long> camp_id { get; set; }
        public Nullable<long> publisher_id { get; set; }
        public IEnumerable<SelectListItem> camp { get; set; }
    }
}