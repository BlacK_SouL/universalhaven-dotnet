using System;
using System.Collections.Generic;

namespace Domain.Models
{
    public partial class fundraisingevent
    {
        public fundraisingevent()
        {
            this.donations = new List<donation>();
        }

        public long id { get; set; }
        public string description { get; set; }
        public Nullable<System.DateTime> finishingDate { get; set; }
        public double goal { get; set; }
        public string imagePath { get; set; }
        public Nullable<System.DateTime> publishDate { get; set; }
        public string state { get; set; }
        public string title { get; set; }
        public string urgency { get; set; }
        public Nullable<long> camp_id { get; set; }
        public Nullable<long> publisher_id { get; set; }
        public virtual camp camp { get; set; }
        public virtual ICollection<donation> donations { get; set; }
        public virtual person person { get; set; }
    }
}
