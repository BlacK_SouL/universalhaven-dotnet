using System;
using System.Collections.Generic;

namespace Domain.Models
{
    public partial class t_todo
    {
        public int id { get; set; }
        public string text { get; set; }
    }
}
