using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace Domain.Models
{
    public enum TaskPriority
    {
        HIGH,LOW, MEDIUM
    }
    public enum TaskStatus
    {
        PENDING,DOING,DONE,DECLINED
    }
    public partial class Task
    {
        public Task()
        {
            this.TaskComments = new List<TaskComment>();
            this.StartingDate = DateTime.Now;
            this.Status = TaskStatus.PENDING;
        }
        
        public long TaskId { get; set; }
        public DateTime DeadLine { get; set; }
        public string Description { get; set; }
        public Nullable<System.DateTime> EndingDate { get; set; }
        public TaskPriority priority { get; set; }
        public DateTime StartingDate { get; set; }
        public TaskStatus Status { get; set; }
        public Nullable<long> campId { get; set; }

        
        public Nullable<long> TaskAssignerId { get; set; }
        [ForeignKey("TaskAssignerId")]
        
        public virtual person TaskAssigner { get; set; }

        
        public Nullable<long> TaskExecutorId { get; set; }
        [ForeignKey("TaskExecutorId")]
        public virtual person TaskExecutor { get; set; }


        [ForeignKey("campId")]
        public virtual camp camp { get; set; }
       
        
        public virtual ICollection<TaskComment> TaskComments { get; set; }
    }
}
