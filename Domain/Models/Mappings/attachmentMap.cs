using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
namespace Domain.Models.Mappings
{
    public class attachmentMap : EntityTypeConfiguration<attachment>
    {
        public attachmentMap()
        {
            // Primary Key
            this.HasKey(t => t.id);

            // Properties
            this.Property(t => t.id)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.name)
                .HasMaxLength(255);

            // Table & Column Mappings
            this.ToTable("attachment", "universalhaven");
            this.Property(t => t.id).HasColumnName("id");
            this.Property(t => t.name).HasColumnName("name");
            this.Property(t => t.applicationFrom_id).HasColumnName("applicationFrom_id");

            // Relationships
            this.HasOptional(t => t.applicationform)
                .WithMany(t => t.attachments)
                .HasForeignKey(d => d.applicationFrom_id);

        }
    }
}
