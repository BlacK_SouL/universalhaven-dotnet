using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Domain.Models.Mappings
{
    public class campMap : EntityTypeConfiguration<camp>
    {
        public campMap()
        {
            // Primary Key
            this.HasKey(t => t.id);

            // Properties
            this.Property(t => t.address)
                .HasMaxLength(255);

            this.Property(t => t.country)
                .HasMaxLength(255);

            this.Property(t => t.latLng)
                .HasMaxLength(255);

            this.Property(t => t.name)
                .HasMaxLength(255);

            // Table & Column Mappings
            this.ToTable("camp", "universalhaven");
            this.Property(t => t.id).HasColumnName("id");
            this.Property(t => t.address).HasColumnName("address");
            this.Property(t => t.budget).HasColumnName("budget");
            this.Property(t => t.capacity).HasColumnName("capacity");
            this.Property(t => t.closingDate).HasColumnName("closingDate");
            this.Property(t => t.country).HasColumnName("country");
            this.Property(t => t.creationDate).HasColumnName("creationDate");
            this.Property(t => t.electricity).HasColumnName("electricity");
            this.Property(t => t.latLng).HasColumnName("latLng");
            this.Property(t => t.name).HasColumnName("name");
            this.Property(t => t.occupancy).HasColumnName("occupancy");
            this.Property(t => t.superficy).HasColumnName("superficy");
            this.Property(t => t.water).HasColumnName("water");
            this.Property(t => t.campCreator_id).HasColumnName("campCreator_id");
            this.Property(t => t.campManager_id).HasColumnName("campManager_id");

            // Relationships
            this.HasOptional(t => t.person)
                .WithMany(t => t.camps)
                .HasForeignKey(d => d.campManager_id);
            this.HasOptional(t => t.person1)
                .WithMany(t => t.camps1)
                .HasForeignKey(d => d.campCreator_id);

        }
    }
}
