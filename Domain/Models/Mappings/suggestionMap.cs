using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Domain.Models.Mappings
{
    public class suggestionMap : EntityTypeConfiguration<suggestion>
    {
        public suggestionMap()
        {
            // Primary Key
            this.HasKey(t => t.id);

            // Properties
            this.Property(t => t.description)
                .HasMaxLength(255);

            this.Property(t => t.title)
                .HasMaxLength(255);

            // Table & Column Mappings
            this.ToTable("suggestion", "universalhaven");
            this.Property(t => t.id).HasColumnName("id");
            this.Property(t => t.description).HasColumnName("description");
            this.Property(t => t.title).HasColumnName("title");
            this.Property(t => t.camp_id).HasColumnName("camp_id");
            this.Property(t => t.publisher_id).HasColumnName("publisher_id");

            // Relationships
            this.HasOptional(t => t.camp)
                .WithMany(t => t.suggestions)
                .HasForeignKey(d => d.camp_id);
            this.HasOptional(t => t.person)
                .WithMany(t => t.suggestions)
                .HasForeignKey(d => d.publisher_id);

        }
    }
}
