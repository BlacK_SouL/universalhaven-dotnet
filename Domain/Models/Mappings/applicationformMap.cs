using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Domain.Models.Mappings
{
    public class applicationformMap : EntityTypeConfiguration<applicationform>
    {
        public applicationformMap()
        {
            // Primary Key
            this.HasKey(t => t.id);

            // Properties
            this.Property(t => t.country)
                .HasMaxLength(255);

            this.Property(t => t.email)
                .HasMaxLength(255);

            this.Property(t => t.gender)
                .HasMaxLength(255);

            this.Property(t => t.godFatherEmail)
                .HasMaxLength(255);

            this.Property(t => t.name)
                .HasMaxLength(255);

            this.Property(t => t.phoneNumber)
                .HasMaxLength(255);

            this.Property(t => t.skills)
                .HasMaxLength(255);

            // Table & Column Mappings
            this.ToTable("applicationform", "universalhaven");
            this.Property(t => t.id).HasColumnName("id");
            this.Property(t => t.accepted).HasColumnName("accepted");
            this.Property(t => t.country).HasColumnName("country");
            this.Property(t => t.email).HasColumnName("email");
            this.Property(t => t.gender).HasColumnName("gender");
            this.Property(t => t.godFatherEmail).HasColumnName("godFatherEmail");
            this.Property(t => t.name).HasColumnName("name");
            this.Property(t => t.phoneNumber).HasColumnName("phoneNumber");
            this.Property(t => t.skills).HasColumnName("skills");
            this.Property(t => t.submissionDate).HasColumnName("submissionDate");
            this.Property(t => t.reviewer_id).HasColumnName("reviewer_id");

            // Relationships
            this.HasOptional(t => t.person)
                .WithMany(t => t.applicationforms)
                .HasForeignKey(d => d.reviewer_id);

        }
    }
}
