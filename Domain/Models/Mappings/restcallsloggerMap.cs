using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Domain.Models.Mappings
{
    public class restcallsloggerMap : EntityTypeConfiguration<restcallslogger>
    {
        public restcallsloggerMap()
        {
            // Primary Key
            this.HasKey(t => t.id);

            // Properties
            this.Property(t => t.endpoint)
                .HasMaxLength(255);

            // Table & Column Mappings
            this.ToTable("restcallslogger", "universalhaven");
            this.Property(t => t.id).HasColumnName("id");
            this.Property(t => t.callDate).HasColumnName("callDate");
            this.Property(t => t.endpoint).HasColumnName("endpoint");
            this.Property(t => t.user_id).HasColumnName("user_id");

            // Relationships
            this.HasOptional(t => t.person)
                .WithMany(t => t.restcallsloggers)
                .HasForeignKey(d => d.user_id);

        }
    }
}
