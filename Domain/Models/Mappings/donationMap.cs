using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Domain.Models.Mappings
{
    public class donationMap : EntityTypeConfiguration<donation>
    {
        public donationMap()
        {
            // Primary Key
            this.HasKey(t => t.id);

            // Properties
            this.Property(t => t.contributorAddress)
                .HasMaxLength(255);

            this.Property(t => t.contributorEmail)
                .HasMaxLength(255);

            this.Property(t => t.contributorName)
                .HasMaxLength(255);

            this.Property(t => t.country)
                .HasMaxLength(255);

            this.Property(t => t.paymentMethod)
                .HasMaxLength(255);

            this.Property(t => t.transactionReference)
                .HasMaxLength(255);

            // Table & Column Mappings
            this.ToTable("donation", "universalhaven");
            this.Property(t => t.id).HasColumnName("id");
            this.Property(t => t.amount).HasColumnName("amount");
            this.Property(t => t.contributorAddress).HasColumnName("contributorAddress");
            this.Property(t => t.contributorEmail).HasColumnName("contributorEmail");
            this.Property(t => t.contributorName).HasColumnName("contributorName");
            this.Property(t => t.country).HasColumnName("country");
            this.Property(t => t.donationDate).HasColumnName("donationDate");
            this.Property(t => t.paymentMethod).HasColumnName("paymentMethod");
            this.Property(t => t.transactionReference).HasColumnName("transactionReference");
            this.Property(t => t.fundraisingEvent_id).HasColumnName("fundraisingEvent_id");

            // Relationships
            this.HasOptional(t => t.fundraisingevent)
                .WithMany(t => t.donations)
                .HasForeignKey(d => d.fundraisingEvent_id);

        }
    }
}
