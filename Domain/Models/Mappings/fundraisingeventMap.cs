using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Domain.Models.Mappings
{
    public class fundraisingeventMap : EntityTypeConfiguration<fundraisingevent>
    {
        public fundraisingeventMap()
        {
            // Primary Key
            this.HasKey(t => t.id);

            // Properties
            this.Property(t => t.description)
                .HasMaxLength(255);

            this.Property(t => t.imagePath)
                .HasMaxLength(255);

            this.Property(t => t.state)
                .HasMaxLength(255);

            this.Property(t => t.title)
                .HasMaxLength(255);

            this.Property(t => t.urgency)
                .HasMaxLength(255);

            // Table & Column Mappings
            this.ToTable("fundraisingevent", "universalhaven");
            this.Property(t => t.id).HasColumnName("id");
            this.Property(t => t.description).HasColumnName("description");
            this.Property(t => t.finishingDate).HasColumnName("finishingDate");
            this.Property(t => t.goal).HasColumnName("goal");
            this.Property(t => t.imagePath).HasColumnName("imagePath");
            this.Property(t => t.publishDate).HasColumnName("publishDate");
            this.Property(t => t.state).HasColumnName("state");
            this.Property(t => t.title).HasColumnName("title");
            this.Property(t => t.urgency).HasColumnName("urgency");
            this.Property(t => t.camp_id).HasColumnName("camp_id");
            this.Property(t => t.publisher_id).HasColumnName("publisher_id");

            // Relationships
            this.HasOptional(t => t.camp)
                .WithMany(t => t.fundraisingevents)
                .HasForeignKey(d => d.camp_id);
            this.HasOptional(t => t.person)
                .WithMany(t => t.fundraisingevents)
                .HasForeignKey(d => d.publisher_id);

        }
    }
}
