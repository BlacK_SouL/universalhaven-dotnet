using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Domain.Models.Mappings
{
    public class resourceMap : EntityTypeConfiguration<resource>
    {
        public resourceMap()
        {
            // Primary Key
            this.HasKey(t => t.id);

            // Properties
            this.Property(t => t.name)
                .HasMaxLength(255);

            this.Property(t => t.type)
                .HasMaxLength(255);

            this.Property(t => t.unit)
                .HasMaxLength(255);

            // Table & Column Mappings
            this.ToTable("resource", "universalhaven");
            this.Property(t => t.id).HasColumnName("id");
            this.Property(t => t.name).HasColumnName("name");
            this.Property(t => t.quantity).HasColumnName("quantity");
            this.Property(t => t.type).HasColumnName("type");
            this.Property(t => t.unit).HasColumnName("unit");
            this.Property(t => t.camp_id).HasColumnName("camp_id");

            // Relationships
            this.HasOptional(t => t.camp)
                .WithMany(t => t.resources)
                .HasForeignKey(d => d.camp_id);

        }
    }
}
