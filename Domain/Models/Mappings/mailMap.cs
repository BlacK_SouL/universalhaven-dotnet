using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
namespace Domain.Models.Mappings
{
    public class mailMap : EntityTypeConfiguration<mail>
    {
        public mailMap()
        {
            // Primary Key
            this.HasKey(t => t.id);

            // Properties
            this.Property(t => t.Mailsender)
                .HasMaxLength(255);

            this.Property(t => t.content)
                .HasMaxLength(255);

            this.Property(t => t.subject)
                .HasMaxLength(255);

            // Table & Column Mappings
            this.ToTable("mail", "universalhaven");
            this.Property(t => t.id).HasColumnName("id");
            this.Property(t => t.Mailsender).HasColumnName("Mailsender");
            this.Property(t => t.content).HasColumnName("content");
            this.Property(t => t.subject).HasColumnName("subject");
        }
    }
}
