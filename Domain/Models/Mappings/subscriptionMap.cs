using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Domain.Models.Mappings
{
    public class subscriptionMap : EntityTypeConfiguration<subscription>
    {
        public subscriptionMap()
        {
            // Primary Key
            this.HasKey(t => t.id);

            // Properties
            this.Property(t => t.customer_id)
                .HasMaxLength(255);

            this.Property(t => t.email)
                .HasMaxLength(255);

            this.Property(t => t.name)
                .HasMaxLength(255);

            this.Property(t => t.plan)
                .HasMaxLength(255);

            // Table & Column Mappings
            this.ToTable("subscription", "universalhaven");
            this.Property(t => t.id).HasColumnName("id");
            this.Property(t => t.canceled).HasColumnName("canceled");
            this.Property(t => t.customer_id).HasColumnName("customer_id");
            this.Property(t => t.email).HasColumnName("email");
            this.Property(t => t.name).HasColumnName("name");
            this.Property(t => t.plan).HasColumnName("plan");
            this.Property(t => t.subscriptionDate).HasColumnName("subscriptionDate");
        }
    }
}
