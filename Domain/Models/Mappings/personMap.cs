using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Domain.Models.Mappings
{
    public class personMap : EntityTypeConfiguration<person>
    {
        public personMap()
        {
            // Primary Key
            this.HasKey(t => t.id);

            // Properties
            this.Property(t => t.type)
                .IsRequired()
                .HasMaxLength(31);

            this.Property(t => t.country)
                .HasMaxLength(255);

            this.Property(t => t.gender)
                .HasMaxLength(255);

            this.Property(t => t.name)
                .HasMaxLength(255);

            this.Property(t => t.surname)
                .HasMaxLength(255);

            this.Property(t => t.emergencyContact)
                .HasMaxLength(255);

            this.Property(t => t.identityNumber)
                .HasMaxLength(255);

            this.Property(t => t.maritalStatus)
                .HasMaxLength(255);

            this.Property(t => t.specialNeeds)
                .HasMaxLength(255);

            this.Property(t => t.address)
                .HasMaxLength(255);

            this.Property(t => t.email)
                .HasMaxLength(255);

            this.Property(t => t.login)
                .HasMaxLength(255);

            this.Property(t => t.motivation)
                .HasMaxLength(255);

            this.Property(t => t.password)
                .HasMaxLength(255);

            this.Property(t => t.profession)
                .HasMaxLength(255);

            this.Property(t => t.role)
                .HasMaxLength(255);

            this.Property(t => t.skills)
                .HasMaxLength(255);

            this.Property(t => t.token)
                .HasMaxLength(255);

            this.Property(t => t.picture)
                .HasMaxLength(255);

            // Table & Column Mappings
            this.ToTable("person", "universalhaven");
            this.Property(t => t.type).HasColumnName("type");
            this.Property(t => t.id).HasColumnName("id");
            this.Property(t => t.birthDate).HasColumnName("birthDate");
            this.Property(t => t.country).HasColumnName("country");
            this.Property(t => t.gender).HasColumnName("gender");
            this.Property(t => t.name).HasColumnName("name");
            this.Property(t => t.surname).HasColumnName("surname");
            this.Property(t => t.emergencyContact).HasColumnName("emergencyContact");
            this.Property(t => t.identityNumber).HasColumnName("identityNumber");
            this.Property(t => t.identityType).HasColumnName("identityType");
            this.Property(t => t.maritalStatus).HasColumnName("maritalStatus");
            this.Property(t => t.specialNeeds).HasColumnName("specialNeeds");
            this.Property(t => t.address).HasColumnName("address");
            this.Property(t => t.email).HasColumnName("email");
            this.Property(t => t.isActif).HasColumnName("isActif");
            this.Property(t => t.login).HasColumnName("login");
            this.Property(t => t.motivation).HasColumnName("motivation");
            this.Property(t => t.password).HasColumnName("password");
            this.Property(t => t.profession).HasColumnName("profession");
            this.Property(t => t.role).HasColumnName("role");
            this.Property(t => t.skills).HasColumnName("skills");
            this.Property(t => t.subscribed).HasColumnName("subscribed");
            this.Property(t => t.subscriptionDate).HasColumnName("subscriptionDate");
            this.Property(t => t.token).HasColumnName("token");
            this.Property(t => t.camp_id).HasColumnName("camp_id");
            this.Property(t => t.assignedCamp_id).HasColumnName("assignedCamp_id");
            this.Property(t => t.picture).HasColumnName("picture");

            // Relationships
            this.HasOptional(t => t.camp)
                .WithMany(t => t.people)
                .HasForeignKey(d => d.assignedCamp_id);
            this.HasOptional(t => t.camp1)
                .WithMany(t => t.people1)
                .HasForeignKey(d => d.camp_id);
            

        }
    }
}
