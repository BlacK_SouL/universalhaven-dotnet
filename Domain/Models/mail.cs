using System;
using System.Collections.Generic;

namespace Domain.Models
{
    public partial class mail
    {
        public long id { get; set; }
        public string Mailsender { get; set; }
        public string content { get; set; }
        public string subject { get; set; }
    }
}
