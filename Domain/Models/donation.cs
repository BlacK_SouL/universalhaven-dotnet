using System;
using System.Collections.Generic;

namespace Domain.Models
{
    public partial class donation
    {
        public long id { get; set; }
        public double amount { get; set; }
        public string contributorAddress { get; set; }
        public string contributorEmail { get; set; }
        public string contributorName { get; set; }
        public string country { get; set; }
        public Nullable<System.DateTime> donationDate { get; set; }
        public string paymentMethod { get; set; }
        public string transactionReference { get; set; }
        public Nullable<long> fundraisingEvent_id { get; set; }
        public virtual fundraisingevent fundraisingevent { get; set; }
    }
}
