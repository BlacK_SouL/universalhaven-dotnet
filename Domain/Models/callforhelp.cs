using System;
using System.Collections.Generic;

namespace Domain.Models
{
    public partial class callforhelp
    {
        public long id { get; set; }
        public bool active { get; set; }
        public string address { get; set; }
        public Nullable<System.DateTime> creationDate { get; set; }
        public string description { get; set; }
        public string imagePath { get; set; }
        public string title { get; set; }
        public Nullable<long> camp_id { get; set; }
        public Nullable<long> publisher_id { get; set; }
        public virtual camp camp { get; set; }
        public virtual person person { get; set; }
    }
}
