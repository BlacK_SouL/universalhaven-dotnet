﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Models
{
    public enum typeroom
    {
        Couple,Male,Female, Families
    }
    public partial class room
    {
        [Key]
        public int roomId { get; set; }
        public typeroom Type { get; set; }
        public int remainingbeds { get; set; }
        public int beds { get; set; }
        public string reference { get; set; }
        public double superficy { get; set; }
        public long campId { get; set; }
        [ForeignKey("campId")]
        public camp Camp;
    }
}