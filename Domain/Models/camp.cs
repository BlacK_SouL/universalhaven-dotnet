using System;
using System.Collections.Generic;

namespace Domain.Models
{
    public partial class camp
    {
        public camp()
        {
            this.callforhelps = new List<callforhelp>();
            this.resources = new List<resource>();
            this.people = new List<person>();
            this.tasks = new List<Task>();
            this.fundraisingevents = new List<fundraisingevent>();
            this.suggestions = new List<suggestion>();
            this.people1 = new List<person>();
        }
        
        public long id { get; set; }
        public string address { get; set; }
        public double budget { get; set; }
        public int capacity { get; set; }
        public Nullable<System.DateTime> closingDate { get; set; }
        public string country { get; set; }
        public Nullable<System.DateTime> creationDate { get; set; }
        public bool electricity { get; set; }
        public string latLng { get; set; }
        public string name { get; set; }
        public int occupancy { get; set; }
        public double superficy { get; set; }
        public bool water { get; set; } 
        public Nullable<long> campCreator_id { get; set; }
        public Nullable<long> campManager_id { get; set; }
        public virtual ICollection<callforhelp> callforhelps { get; set; }
        public virtual ICollection<resource> resources { get; set; }
        public virtual ICollection<person> people { get; set; }
        public virtual ICollection<Task> tasks { get; set; }
        public virtual person person { get; set; }
        public virtual person person1 { get; set; }
        public virtual ICollection<fundraisingevent> fundraisingevents { get; set; }
        public virtual ICollection<suggestion> suggestions { get; set; }
        public virtual ICollection<person> people1 { get; set; }
        

    }
}
