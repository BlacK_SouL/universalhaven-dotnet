using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Domain.Models
{
    public enum identityTypes
    {
        Passport, Identity_Card, Drive_License
    }
    public partial class person
    {
        public person()
        {
            this.applicationforms = new List<applicationform>();
            this.callforhelps = new List<callforhelp>();
            this.camps = new List<camp>();
            this.camps1 = new List<camp>();
            this.fundraisingevents = new List<fundraisingevent>();
            this.tasks = new List<Task>();
            this.taskcomments = new List<TaskComment>();
            this.suggestions = new List<suggestion>();
            this.restcallsloggers = new List<restcallslogger>();
            this.tasks1 = new List<Task>();
        }
        public int roomId { get; set; }
        [ForeignKey("roomId")]
        public room Room;

        public string type { get; set; }
       
        public long id { get; set; }
        public long birthDate { get; set; }
        public string country { get; set; }
        public string gender { get; set; }
        public string name { get; set; }
        public string surname { get; set; }
        public string emergencyContact { get; set; }
        public string identityNumber { get; set; }
        public Nullable<int> identityType { get; set; }
        public string maritalStatus { get; set; }
        public string specialNeeds { get; set; }
        public string address { get; set; }
        public string email { get; set; }
        public Nullable<bool> isActif { get; set; }
        public string login { get; set; }
        public string motivation { get; set; }
        public string password { get; set; }
        public string profession { get; set; }
        public string role { get; set; }
        public string skills { get; set; }
        public Nullable<bool> subscribed { get; set; }
        public long subscriptionDate { get; set; }
        public string token { get; set; }
        public Nullable<long> camp_id { get; set; }
        public Nullable<long> assignedCamp_id { get; set; }
        public string picture { get; set; }
        public virtual ICollection<applicationform> applicationforms { get; set; }
        public virtual ICollection<callforhelp> callforhelps { get; set; }
        public virtual camp camp { get; set; }
        public virtual ICollection<camp> camps { get; set; }
        public virtual ICollection<camp> camps1 { get; set; }
        public virtual camp camp1 { get; set; }
        public virtual ICollection<fundraisingevent> fundraisingevents { get; set; }
        public virtual ICollection<Task> tasks { get; set; }
        public virtual ICollection<TaskComment> taskcomments { get; set; }
        public virtual ICollection<suggestion> suggestions { get; set; }
        public virtual ICollection<restcallslogger> restcallsloggers { get; set; }
        public virtual ICollection<Task> tasks1 { get; set; }
    }
}
