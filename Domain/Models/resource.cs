using System;
using System.Collections.Generic;

namespace Domain.Models
{
    public partial class resource
    {
        public long id { get; set; }
        public string name { get; set; }
        public double quantity { get; set; }
        public string type { get; set; }
        public string unit { get; set; }
        public Nullable<long> camp_id { get; set; }
        public virtual camp camp { get; set; }
    }
}
