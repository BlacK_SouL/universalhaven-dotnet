using System;
using System.Collections.Generic;

namespace Domain.Models
{
    public partial class applicationform
    {
        public applicationform()
        {
            this.attachments = new List<attachment>();
        }

        public long id { get; set; }
        public Nullable<bool> accepted { get; set; }
        public string country { get; set; }
        public string email { get; set; }
        public string gender { get; set; }
        public string godFatherEmail { get; set; }
        public string name { get; set; }
        public string phoneNumber { get; set; }
        public string skills { get; set; }
        public Nullable<System.DateTime> submissionDate { get; set; }
        public Nullable<long> reviewer_id { get; set; }
        public virtual ICollection<attachment> attachments { get; set; }
        public virtual person person { get; set; }
    }
}
