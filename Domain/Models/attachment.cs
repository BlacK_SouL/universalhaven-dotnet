using System;
using System.Collections.Generic;

namespace Domain.Models
{
    public partial class attachment
    {
        public int id { get; set; }
        public string name { get; set; }
        public Nullable<long> applicationFrom_id { get; set; }
        public virtual applicationform applicationform { get; set; }
    }
}
