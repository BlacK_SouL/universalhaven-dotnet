using System;
using System.Collections.Generic;

namespace Domain.Models
{
    public partial class restcallslogger
    {
        public long id { get; set; }
        public Nullable<System.DateTime> callDate { get; set; }
        public string endpoint { get; set; }
        public Nullable<long> user_id { get; set; }
        public virtual person person { get; set; }
    }
}
