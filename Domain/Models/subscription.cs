using System;
using System.Collections.Generic;

namespace Domain.Models
{
    public partial class subscription
    {
        public long id { get; set; }
        public bool canceled { get; set; }
        public string customer_id { get; set; }
        public string email { get; set; }
        public string name { get; set; }
        public string plan { get; set; }
        public Nullable<System.DateTime> subscriptionDate { get; set; }
    }
}
