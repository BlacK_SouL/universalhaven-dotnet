using System;
using System.Collections.Generic;

namespace Domain.Models
{
    public partial class suggestion
    {
        public long id { get; set; }
        public string description { get; set; }
        public string title { get; set; }
        public Nullable<long> camp_id { get; set; }
        public Nullable<long> publisher_id { get; set; }
        public virtual camp camp { get; set; }
        public virtual person person { get; set; }
    }
}
