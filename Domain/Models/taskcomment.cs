using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace Domain.Models
{
    public partial class TaskComment
    {
        public long Id { get; set; }
        public string Content { get; set; }
        public DateTime PublishDate { get; set; }


        
        public Nullable<long> PublisherId { get; set; }
        [ForeignKey("PublisherId")]
        public virtual person Publisher { get; set; }

        public Nullable<long> TaskId { get; set; }

        [ForeignKey("TaskId")]
        public virtual Task Task { get; set; }
    }
}
