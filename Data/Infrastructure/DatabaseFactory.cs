﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Data;

namespace Universalhaven.Data.Infrastructure
{
    public class DatabaseFactory : Disposable, IDatabaseFactory
    {
        private universalhavenContext dataContext;
        public universalhavenContext DataContext { get { return dataContext; } }
        public DatabaseFactory()
        {
            dataContext = new universalhavenContext();
        }
        protected override void DisposeCore()
        {
            if (DataContext != null)
                DataContext.Dispose();
        }
    }


}
