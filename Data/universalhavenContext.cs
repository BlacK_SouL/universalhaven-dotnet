using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using Domain.Models;
using Domain.Models.Mappings;
using MySql.Data.Entity;
using System.Data.Common;

namespace Data
{
    [DbConfigurationType(typeof(MySqlEFConfiguration))]
    public partial class universalhavenContext : DbContext
    {
        
        static universalhavenContext()
        {
            //Database.SetInitializer<universalhavenContext>(null);
        }

        public universalhavenContext()
            : base("Name=universalhavenContext")
        {
            
        }
        
        public Database getDatabase()
        {
            return Database;
        }

        public DbSet<applicationform> applicationforms { get; set; }
        public DbSet<attachment> attachments { get; set; }
        public DbSet<callforhelp> callforhelps { get; set; }
        public DbSet<camp> camps { get; set; }
        public DbSet<donation> donations { get; set; }
        public DbSet<fundraisingevent> fundraisingevents { get; set; }
        public DbSet<mail> mails { get; set; }
        public DbSet<person> people { get; set; }
        public DbSet<resource> resources { get; set; }
        public DbSet<restcallslogger> restcallsloggers { get; set; }
        public DbSet<subscription> subscriptions { get; set; }
        public DbSet<suggestion> suggestions { get; set; }
        public DbSet<t_todo> t_todo { get; set; }
        public DbSet<Task> tasks { get; set; }
        public DbSet<TaskComment> taskcomments { get; set; }
        public DbSet<room> rooms { get; set; }
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Configurations.Add(new applicationformMap());
            modelBuilder.Configurations.Add(new attachmentMap());
            modelBuilder.Configurations.Add(new callforhelpMap());
            modelBuilder.Configurations.Add(new campMap());
            modelBuilder.Configurations.Add(new donationMap());
            modelBuilder.Configurations.Add(new fundraisingeventMap());
            modelBuilder.Configurations.Add(new mailMap());
            modelBuilder.Configurations.Add(new personMap());
            modelBuilder.Configurations.Add(new resourceMap());
            modelBuilder.Configurations.Add(new restcallsloggerMap());
            modelBuilder.Configurations.Add(new subscriptionMap());
            modelBuilder.Configurations.Add(new suggestionMap());
            modelBuilder.Configurations.Add(new t_todoMap());
           
            
        }
    }
}
