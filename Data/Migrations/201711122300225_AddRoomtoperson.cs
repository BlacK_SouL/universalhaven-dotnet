namespace Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddRoomtoperson : DbMigration
    {
        public override void Up()
        {
            AddColumn("person", "roomId", c => c.Int(nullable: true));
            AddForeignKey("dbo.person", "roomId", "dbo.rooms", "roomId");
        }

        public override void Down()
        {
            DropColumn("person", "roomId");
        }
    }
}
