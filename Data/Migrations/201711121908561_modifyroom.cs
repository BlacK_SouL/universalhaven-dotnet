namespace Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class modifyroom : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.rooms", "remainingbeds", c => c.Int(nullable: true , defaultValue :0));
        }
        
        public override void Down()
        {
            DropColumn("dbo.rooms", "remainingbeds");
        }
    }
}
