namespace Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class testintegration : DbMigration
    {
        public override void Up()
        {
            RenameTable(name: "universalhaven.taskcomment", newName: "TaskComments");
            RenameTable(name: "universalhaven.task", newName: "Tasks");
            MoveTable(name: "universalhaven.TaskComments", newSchema: "dbo");
            MoveTable(name: "universalhaven.Tasks", newSchema: "dbo");
            DropForeignKey("universalhaven.taskcomment", "task_id", "universalhaven.task");
            RenameColumn(table: "dbo.TaskComments", name: "publisher_id", newName: "PublisherId");
            RenameColumn(table: "dbo.Tasks", name: "taskExecutor_id", newName: "person_id");
            RenameColumn(table: "dbo.Tasks", name: "taskAssigner_id", newName: "person_id1");
            RenameColumn(table: "dbo.Tasks", name: "camp_id", newName: "campId");
            RenameColumn(table: "dbo.TaskComments", name: "task_id", newName: "TaskId");
            RenameIndex(table: "dbo.Tasks", name: "IX_camp_id", newName: "IX_campId");
            RenameIndex(table: "dbo.Tasks", name: "IX_taskExecutor_id", newName: "IX_person_id");
            RenameIndex(table: "dbo.Tasks", name: "IX_taskAssigner_id", newName: "IX_person_id1");
            RenameIndex(table: "dbo.TaskComments", name: "IX_publisher_id", newName: "IX_PublisherId");
            RenameIndex(table: "dbo.TaskComments", name: "IX_task_id", newName: "IX_TaskId");
            DropPrimaryKey("dbo.Tasks");
            AddColumn("universalhaven.person", "roomId", c => c.Int(nullable: false));
            AddColumn("dbo.rooms", "remainingbeds", c => c.Int(nullable: false));
            AddColumn("dbo.rooms", "reference", c => c.String(unicode: false));
            AddColumn("dbo.Tasks", "TaskId", c => c.Long(nullable: false, identity: true));
            AddColumn("dbo.Tasks", "TaskAssignerId", c => c.Long());
            AddColumn("dbo.Tasks", "TaskExecutorId", c => c.Long());
            AlterColumn("dbo.TaskComments", "Content", c => c.String(unicode: false));
            AlterColumn("dbo.TaskComments", "PublishDate", c => c.DateTime(nullable: false, precision: 0));
            AlterColumn("dbo.Tasks", "DeadLine", c => c.DateTime(nullable: false, precision: 0));
            AlterColumn("dbo.Tasks", "Description", c => c.String(unicode: false));
            AlterColumn("dbo.Tasks", "priority", c => c.Int(nullable: false));
            AlterColumn("dbo.Tasks", "StartingDate", c => c.DateTime(nullable: false, precision: 0));
            AlterColumn("dbo.Tasks", "Status", c => c.Int(nullable: false));
            AddPrimaryKey("dbo.Tasks", "TaskId");
            CreateIndex("dbo.Tasks", "TaskAssignerId");
            CreateIndex("dbo.Tasks", "TaskExecutorId");
            AddForeignKey("dbo.Tasks", "TaskAssignerId", "universalhaven.person", "id");
            AddForeignKey("dbo.Tasks", "TaskExecutorId", "universalhaven.person", "id");
            AddForeignKey("dbo.TaskComments", "TaskId", "dbo.Tasks", "TaskId");
            DropColumn("dbo.Tasks", "id");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Tasks", "id", c => c.Long(nullable: false, identity: true));
            DropForeignKey("dbo.TaskComments", "TaskId", "dbo.Tasks");
            DropForeignKey("dbo.Tasks", "TaskExecutorId", "universalhaven.person");
            DropForeignKey("dbo.Tasks", "TaskAssignerId", "universalhaven.person");
            DropIndex("dbo.Tasks", new[] { "TaskExecutorId" });
            DropIndex("dbo.Tasks", new[] { "TaskAssignerId" });
            DropPrimaryKey("dbo.Tasks");
            AlterColumn("dbo.Tasks", "Status", c => c.Int());
            AlterColumn("dbo.Tasks", "StartingDate", c => c.DateTime(precision: 0));
            AlterColumn("dbo.Tasks", "priority", c => c.Int());
            AlterColumn("dbo.Tasks", "Description", c => c.String(maxLength: 255, storeType: "nvarchar"));
            AlterColumn("dbo.Tasks", "DeadLine", c => c.DateTime(precision: 0));
            AlterColumn("dbo.TaskComments", "PublishDate", c => c.DateTime(precision: 0));
            AlterColumn("dbo.TaskComments", "Content", c => c.String(maxLength: 255, storeType: "nvarchar"));
            DropColumn("dbo.Tasks", "TaskExecutorId");
            DropColumn("dbo.Tasks", "TaskAssignerId");
            DropColumn("dbo.Tasks", "TaskId");
            DropColumn("dbo.rooms", "reference");
            DropColumn("dbo.rooms", "remainingbeds");
            DropColumn("universalhaven.person", "roomId");
            AddPrimaryKey("dbo.Tasks", "id");
            RenameIndex(table: "dbo.TaskComments", name: "IX_TaskId", newName: "IX_task_id");
            RenameIndex(table: "dbo.TaskComments", name: "IX_PublisherId", newName: "IX_publisher_id");
            RenameIndex(table: "dbo.Tasks", name: "IX_person_id1", newName: "IX_taskAssigner_id");
            RenameIndex(table: "dbo.Tasks", name: "IX_person_id", newName: "IX_taskExecutor_id");
            RenameIndex(table: "dbo.Tasks", name: "IX_campId", newName: "IX_camp_id");
            RenameColumn(table: "dbo.TaskComments", name: "TaskId", newName: "task_id");
            RenameColumn(table: "dbo.Tasks", name: "campId", newName: "camp_id");
            RenameColumn(table: "dbo.Tasks", name: "person_id1", newName: "taskAssigner_id");
            RenameColumn(table: "dbo.Tasks", name: "person_id", newName: "taskExecutor_id");
            RenameColumn(table: "dbo.TaskComments", name: "PublisherId", newName: "publisher_id");
            AddForeignKey("universalhaven.taskcomment", "task_id", "universalhaven.task", "id");
            MoveTable(name: "dbo.Tasks", newSchema: "universalhaven");
            MoveTable(name: "dbo.TaskComments", newSchema: "universalhaven");
            RenameTable(name: "universalhaven.Tasks", newName: "task");
            RenameTable(name: "universalhaven.TaskComments", newName: "taskcomment");
        }
    }
}
