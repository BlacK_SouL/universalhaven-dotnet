namespace Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addrefroom : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.rooms", "reference", c => c.String(unicode: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.rooms", "reference");
        }
    }
}
