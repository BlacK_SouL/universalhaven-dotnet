namespace Data.Migrations
{
    using Domain.Models;
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Data.Entity.Migrations;
    
    public partial class tasks : DbMigration
    {
        
        public override void Up()
        {

            CreateTable(
                "dbo.Tasks",
                c => new
                {
                    TaskId = c.Long(nullable: false, identity: true),
                    DeadLine = c.DateTime(nullable: false, precision: 0),
                    Description = c.String(unicode: false),
                    EndingDate = c.DateTime(precision: 0),
                    priority = c.Int(nullable: false),
                    StartingDate = c.DateTime(nullable: false, precision: 0),
                    Status = c.Int(nullable: false),
                    campId = c.Long(),
                    TaskAssignerId = c.Long(),
                    TaskExecutorId = c.Long(),
                    person_id = c.Long(),
                    person_id1 = c.Long(),
                })
                .PrimaryKey(t => t.TaskId);
            /*.ForeignKey("universalhaven.camp", t => t.campId)
            .ForeignKey("universalhaven.person", t => t.TaskAssignerId)
            .ForeignKey("universalhaven.person", t => t.TaskExecutorId)
            .Index(t => t.campId)
            .Index(t => t.TaskAssignerId)
            .Index(t => t.TaskExecutorId);*/

            AddForeignKey("dbo.Tasks", "campId", "dbo.camp", "id");
            AddForeignKey("dbo.Tasks", "TaskAssignerId", "dbo.person", "id");
            AddForeignKey("dbo.Tasks", "TaskExecutorId", "dbo.person", "id");

            CreateTable(
                "dbo.TaskComments",
                c => new
                {
                    Id = c.Long(nullable: false, identity: true),
                    Content = c.String(unicode: false),
                    PublishDate = c.DateTime(nullable: false, precision: 0),
                    PublisherId = c.Long(),
                    TaskId = c.Long(),
                })
                .PrimaryKey(t => t.Id)
            //.ForeignKey("universalhaven.person", t => t.PublisherId)
            .ForeignKey("dbo.Tasks", t => t.TaskId)
           // .Index(t => t.PublisherId)
            .Index(t => t.TaskId);
            AddForeignKey("dbo.TaskComments", "PublisherId", "dbo.person", "id");


        }
        
        public override void Down()
        {
            DropForeignKey("universalhaven.applicationform", "reviewer_id", "universalhaven.person");
            DropForeignKey("dbo.Tasks", "person_id1", "universalhaven.person");
            DropForeignKey("dbo.Tasks", "person_id", "universalhaven.person");
            DropForeignKey("universalhaven.restcallslogger", "user_id", "universalhaven.person");
            DropForeignKey("universalhaven.person", "camp_id", "universalhaven.camp");
            DropForeignKey("universalhaven.person", "assignedCamp_id", "universalhaven.camp");
            DropForeignKey("universalhaven.callforhelp", "publisher_id", "universalhaven.person");
            DropForeignKey("universalhaven.callforhelp", "camp_id", "universalhaven.camp");
            DropForeignKey("dbo.Tasks", "TaskExecutorId", "universalhaven.person");
            DropForeignKey("dbo.TaskComments", "TaskId", "dbo.Tasks");
            DropForeignKey("dbo.TaskComments", "PublisherId", "universalhaven.person");
            DropForeignKey("dbo.Tasks", "TaskAssignerId", "universalhaven.person");
            DropForeignKey("dbo.Tasks", "campId", "universalhaven.camp");
            DropForeignKey("universalhaven.suggestion", "publisher_id", "universalhaven.person");
            DropForeignKey("universalhaven.suggestion", "camp_id", "universalhaven.camp");
            DropForeignKey("universalhaven.resource", "camp_id", "universalhaven.camp");
            DropForeignKey("universalhaven.camp", "campCreator_id", "universalhaven.person");
            DropForeignKey("universalhaven.camp", "campManager_id", "universalhaven.person");
            DropForeignKey("universalhaven.fundraisingevent", "publisher_id", "universalhaven.person");
            DropForeignKey("universalhaven.donation", "fundraisingEvent_id", "universalhaven.fundraisingevent");
            DropForeignKey("universalhaven.fundraisingevent", "camp_id", "universalhaven.camp");
            DropForeignKey("universalhaven.attachment", "applicationFrom_id", "universalhaven.applicationform");
            DropIndex("universalhaven.restcallslogger", new[] { "user_id" });
            DropIndex("dbo.TaskComments", new[] { "TaskId" });
            DropIndex("dbo.TaskComments", new[] { "PublisherId" });
            DropIndex("dbo.Tasks", new[] { "person_id1" });
            DropIndex("dbo.Tasks", new[] { "person_id" });
            DropIndex("dbo.Tasks", new[] { "TaskExecutorId" });
            DropIndex("dbo.Tasks", new[] { "TaskAssignerId" });
            DropIndex("dbo.Tasks", new[] { "campId" });
            DropIndex("universalhaven.suggestion", new[] { "publisher_id" });
            DropIndex("universalhaven.suggestion", new[] { "camp_id" });
            DropIndex("universalhaven.resource", new[] { "camp_id" });
            DropIndex("universalhaven.donation", new[] { "fundraisingEvent_id" });
            DropIndex("universalhaven.fundraisingevent", new[] { "publisher_id" });
            DropIndex("universalhaven.fundraisingevent", new[] { "camp_id" });
            DropIndex("universalhaven.camp", new[] { "campManager_id" });
            DropIndex("universalhaven.camp", new[] { "campCreator_id" });
            DropIndex("universalhaven.callforhelp", new[] { "publisher_id" });
            DropIndex("universalhaven.callforhelp", new[] { "camp_id" });
            DropIndex("universalhaven.person", new[] { "assignedCamp_id" });
            DropIndex("universalhaven.person", new[] { "camp_id" });
            DropIndex("universalhaven.attachment", new[] { "applicationFrom_id" });
            DropIndex("universalhaven.applicationform", new[] { "reviewer_id" });
            DropTable("universalhaven.t_todo");
            DropTable("universalhaven.subscription");
            DropTable("universalhaven.mail");
            DropTable("universalhaven.restcallslogger");
            DropTable("dbo.TaskComments");
            DropTable("dbo.Tasks");
            DropTable("universalhaven.suggestion");
            DropTable("universalhaven.resource");
            DropTable("universalhaven.donation");
            DropTable("universalhaven.fundraisingevent");
            DropTable("universalhaven.camp");
            DropTable("universalhaven.callforhelp");
            DropTable("universalhaven.person");
            DropTable("universalhaven.attachment");
            DropTable("universalhaven.applicationform");
        }
    }
}
