namespace Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class camproom : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.rooms",
                c => new
                    {
                        roomId = c.Int(nullable: false, identity: true),
                        Type = c.Int(nullable: false),
                        beds = c.Int(nullable: false),
                        superficy = c.Double(nullable: false),
                        campId = c.Long(nullable: false),
                    })
                .PrimaryKey(t => t.roomId);
            AddForeignKey("dbo.rooms", "campId", "dbo.camp","id");
        }
        
        public override void Down()
        {
            DropTable("dbo.rooms");
        }
    }
}
